module.exports = {
  apps: [
    {
      name: 'LMS API - Production',
      script: './dist/main.js',
      instances: 4,
    },
  ],
};
