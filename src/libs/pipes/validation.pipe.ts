import {
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { I18nRequestScopeService } from 'nestjs-i18n';

@Injectable()
export class I18nValidationPipe implements PipeTransform<any> {
  constructor(private readonly i18n: I18nRequestScopeService) {}

  async transform(value, metadata: ArgumentMetadata) {
    if (!value) {
      throw new BadRequestException('No data submitted');
    }

    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new HttpException(
        {
          success: false,
          statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
          message: await this.i18n.translate('message.validationError'),
          errors: await this.buildError(errors),
        },
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
    return value;
  }

  private async buildError(errors) {
    const result = {};
    for await (const el of errors) {
      let prop = el.property;

      const constraints: string = el.constraints ? el.constraints : el.children;

      for await (const constraint of Object.entries(constraints)) {
        const propTranslate = await this.i18n.translate(`field.${el.property}`);

        let textError = await this.i18n.translate(
          `validation.${constraint[0]}`,
          {
            args: { property: propTranslate },
          },
        );

        if (textError.split('validation.').length > 1) {
          textError = constraint[1];
        }

        result[prop] = `${this.convertCamelCaseToSentenceCase(textError)}`;
      }
    }

    return result;
  }

  private convertCamelCaseToSentenceCase(text: string): string {
    let result = text.replace(/([A-Z])/g, ' $1');
    let finalResult = result.charAt(0).toUpperCase() + result.slice(1);

    return finalResult.trim();
  }

  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find((type) => metatype === type);
  }

  async translateErrors(errors: any) {
    const data = [];
    for (let i = 0; i < errors.length; i++) {
      const message = await Promise.all(
        Object.values(errors[i].constraints).map(
          async (value: string) => await this.i18n.translate(value),
        ),
      );
      data.push({ field: errors[i].property, message: message });
    }
    return data;
  }
}
