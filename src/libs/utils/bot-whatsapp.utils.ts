import axios from 'axios';
import { url } from 'inspector';
import {
  WhatsappNumberHostActivity,
  WhatsappNumberHostActivityType,
} from 'src/whatsapp-bots/entities/whatsapp-number-host-activity.entity';
import {
  WhatsappNumberHost,
  WhatsappNumberHostStatus,
  WhatsappNumberHostType,
} from 'src/whatsapp-bots/entities/whatsapp-number-host.entity';

async function createActivity(
  host,
  type,
  payload,
  url,
): Promise<WhatsappNumberHostActivity> {
  const activity = new WhatsappNumberHostActivity();
  activity.host = host;
  activity.url = url;
  activity.type = type;
  activity.payload = JSON.stringify(payload);
  await activity.save();

  return activity;
}

function generateUrl(host, path) {
  return {
    domainUrl: `${host.host}${path}`,
    ipUrl: `http://${host.ipHost}:${host.ipPortHost}${path}`,
  };
}

export async function whatsappBotCreateGroup(
  payload: {
    name: string;
    userPhoneNumbers: string[];
    adminPhoneNumbers?: string[];
    description: string;
    id: string;
    messageOnlyAdmin?: boolean;
    activityId?: string;
    image?: string;
  },
  host: WhatsappNumberHost,
) {
  const { domainUrl, ipUrl } = generateUrl(host, '/v1/groups/create-grup');
  let id = payload.activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.CREATE_GROUP,
      { ...payload },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  }
}

export async function whatsappBotRemoveUserFromGroup(
  groupId: string,
  payload: { userPhoneNumbers: string[]; activityId?: string },
  host: WhatsappNumberHost,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/remove-member`,
  );
  let id = payload.activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.REMOVE_MEMBER,
      { ...payload },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(
      domainUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  } catch (error) {
    return await axios.patch(
      ipUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  }
}

export async function whatsappBotAddUserToGroup(
  groupId: string,
  payload: {
    userPhoneNumbers: string[];
    adminPhoneNumbers?: string[];
    activityId?: string;
  },
  host: WhatsappNumberHost,
) {
  let id = payload.activityId;
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/add-member`,
  );

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.INVITE_MEMBER,
      { ...payload },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(
      domainUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  } catch (error) {
    return await axios.patch(
      ipUrl,
      { ...payload, activityId: id },
      {
        headers: { Authorization: `Bearer ${host.token}` },
      },
    );
  }
}

export async function whatsappBotSetUserToAdmin(
  groupId: string,
  payload: {
    userPhoneNumbers: string[];
    activityId?: string;
  },
  host: WhatsappNumberHost,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/set-admin`,
  );
  let id = payload.activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SET_MEMBER_GROUP_TO_ADMIN,
      { ...payload },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(domainUrl, payload, {
      headers: { Authorization: `Bearer ${host.token}` },
    });
  } catch (error) {
    return await axios.patch(ipUrl, payload, {
      headers: { Authorization: `Bearer ${host.token}` },
    });
  }
}

export async function whatsappBotFetchMemberGroup(
  groupId: string,
  host: WhatsappNumberHost,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/members`,
  );

  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SYNC_MEMBER,
      {},
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.get(domainUrl, {
      params: { activityId: id },
      headers: { Authorization: `Bearer ${host.token}` },
    });
  } catch (error) {
    if (error.response.data.status !== 400) {
      return await axios.get(ipUrl, {
        params: { activityId: id },
        headers: { Authorization: `Bearer ${host.token}` },
      });
    } else {
      return { data: null };
    }
  }
}

export async function whatsappBotFetchChatGroup(
  groupId: string,
  host: WhatsappNumberHost,
  limit: number,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/sync-messages`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SYNC_MESSAGE,
      { limit },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      { limit, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      { limit, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotSyncMemberGroup(
  groupId: string,
  host: WhatsappNumberHost,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/sync-members`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SYNC_MEMBER,
      { groupId },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      { groupId, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      { groupId, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotSetAllowSendMessage(
  groupId: string,
  value: boolean,
  host: WhatsappNumberHost,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/set-only-admin-message`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SET_ALLOW_SEND_MESSAGE_GROUP,
      { value },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(
      domainUrl,
      {
        value,
        activityId: id,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.patch(
      ipUrl,
      {
        value,
        activityId: id,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotSetAllowAdminInfo(
  groupId: string,
  value: boolean,
  host: WhatsappNumberHost,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${groupId}/set-only-admin-info`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SET_ALLOW_CHANGE_INFO_GROUP,
      { value },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(
      domainUrl,
      {
        value,
        activityId: id,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.patch(
      ipUrl,
      {
        value,
        activityId: id,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotSendMessage(
  uniqueCode: string,
  payload: {
    message: string;
    image?: string;
    isBlaster?: boolean;
    activityId?: string;
    broadcastId?: string;
    kulwaHistoryId?: string;
    typeActivity?: string;
  },
  host?: WhatsappNumberHost,
) {
  let whatsappNumberHost = null;

  if (!host) {
    whatsappNumberHost = await WhatsappNumberHost.findOne({
      where: {
        status: WhatsappNumberHostStatus.CONNECTED,
        type: WhatsappNumberHostType.BROADCAST,
        isActive: true,
      },
    });
  } else {
    whatsappNumberHost = host;
  }

  if (!whatsappNumberHost) return;

  const { domainUrl, ipUrl } = generateUrl(
    whatsappNumberHost,
    `/v1/groups/${uniqueCode}/send-message`,
  );
  let id = payload.activityId;

  if (!id) {
    let activity = null;

    try {
      activity = await createActivity(
        whatsappNumberHost,
        payload.typeActivity || WhatsappNumberHostActivityType.SEND_MESSAGE,
        payload,
        domainUrl,
      );
    } catch (error) {
      activity = await createActivity(
        whatsappNumberHost,
        WhatsappNumberHostActivityType.SEND_MESSAGE,
        payload,
        domainUrl,
      );
    }

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      { ...payload, activityId: id },
      { headers: { Authorization: `Bearer ${whatsappNumberHost.token}` } },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      { ...payload, activityId: id },
      { headers: { Authorization: `Bearer ${whatsappNumberHost.token}` } },
    );
  }
}

export async function whatsappBotSendMessageGreeting(
  uniqueCode: string,
  payload: {
    message: string;
    id: string;
    image?: string;
    activityId?: string;
  },
  host: WhatsappNumberHost,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${uniqueCode}/send-message`,
  );
  let id = payload.activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SEND_MESSAGE_GREETING,
      payload,
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      {
        ...payload,
        isBlaster: false,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      {
        ...payload,
        isBlaster: false,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotSendMessageBlaster(
  uniqueCode: string,
  payload: {
    message: string;
    id: string;
    image?: string;
    activityId?: string;
  },
  host: WhatsappNumberHost,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${uniqueCode}/send-message`,
  );
  let id = payload.activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SEND_MESSAGE_BLASTER,
      payload,
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.post(
      domainUrl,
      {
        ...payload,
        isBlaster: true,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.post(
      ipUrl,
      {
        ...payload,
        isBlaster: true,
      },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}

export async function whatsappBotGetMessageStatus(
  uniqueCode: string,
  messageId: string,
  host: WhatsappNumberHost,
  blastetQueueId?: string,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${uniqueCode}/messages/${messageId}/get-chat-message-status?blastetQueueId=${blastetQueueId}`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.GET_MESSAGE_STATUS,
      {},
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.get(domainUrl, {
      params: { activityId: id },
      headers: { Authorization: `Bearer ${host.token}` },
    });
  } catch (error) {
    return await axios.get(ipUrl, {
      params: { activityId: id },
      headers: { Authorization: `Bearer ${host.token}` },
    });
  }
}

export async function whatsappBotSetGroupDescription(
  uniqueCode: string,
  description: string,
  host: WhatsappNumberHost,
  activityId?: string,
) {
  const { domainUrl, ipUrl } = generateUrl(
    host,
    `/v1/groups/${uniqueCode}/set-group-description`,
  );
  let id = activityId;

  if (!id) {
    const activity = await createActivity(
      host,
      WhatsappNumberHostActivityType.SET_GROUP_DESCRIPTION,
      { description },
      domainUrl,
    );

    id = activity.id;
  }

  try {
    return await axios.patch(
      domainUrl,
      { description, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  } catch (error) {
    return await axios.patch(
      ipUrl,
      { description, activityId: id },
      { headers: { Authorization: `Bearer ${host.token}` } },
    );
  }
}
