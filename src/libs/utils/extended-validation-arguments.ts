import { ValidationArguments } from 'class-validator';
import { User } from 'src/users/users.entity';
import { REQUEST_CONTEXT } from './inject-user.interceptor';

export interface ExtendedValidationArguments extends ValidationArguments {
  object: {
    [REQUEST_CONTEXT]: {
      user: User;
    };
  };
}
