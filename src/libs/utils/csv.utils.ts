import slugify from 'slugify';
import { upload } from './upload.utils';
require('dotenv').config();

export async function convertCsvToArray(file, name, returnWithPath?: boolean) {
  const path = await new upload().uploadFile(
    file,
    name,
    slugify(`import-${name}-${new Date()}`),
  );

  const csv = require('csv-parser');
  const fs = require('fs');

  const processCsv = new Promise((resolve, reject) => {
    const results = [];

    fs.createReadStream(path)
      .pipe(csv())
      .on('data', (row) => {
        results.push(row);
      })
      .on('finish', () => {
        resolve(results);
      });
  });

  if (returnWithPath) {
    return {
      results: await processCsv,
      path: `${path}`,
    };
  } else {
    return processCsv;
  }
}

export async function parseChatWhatsappFromCsv(file): Promise<any> {
  const fs = require('fs');
  const whatsapp = require('whatsapp-chat-parser');

  const processCsv = new Promise((resolve, reject) => {
    const fileContents = fs.readFileSync(file, 'utf8');

    whatsapp
      .parseString(fileContents)
      .then((messages) => {
        resolve(messages);
      })
      .catch((err) => {
        // Something went wrong
      });
  });

  return processCsv;
}
