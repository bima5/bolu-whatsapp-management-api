require('dotenv').config();

export function encrypted(text: string): string {
  const CryptoJS = require('crypto-js');
  const encryptedText = CryptoJS.AES.encrypt(
    text,
    process.env.SECRET_TOKEN,
  ).toString();

  return encryptedText;
}

export function decrypted(hashToken): string {
  try {
    const CryptoJS = require('crypto-js');
    const bytes = CryptoJS.AES.decrypt(
      hashToken().toString().split('Bearer')[1].trim(),
      process.env.SECRET_TOKEN,
    );

    console.log(bytes.toString(CryptoJS.enc.Utf8));

    return 'Bearer ' + bytes.toString(CryptoJS.enc.Utf8);
  } catch (error) {
    console.log(error);
    return hashToken;
  }
}
