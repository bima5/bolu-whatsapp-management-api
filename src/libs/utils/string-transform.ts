// +62 -> 62
// 08 -> 628
export function convertToPhoneNumber(phoneNumber: string) {
  try {
    const phoneNumberArray = phoneNumber.split('');
    let phone = phoneNumber;

    if (phoneNumberArray[0] === '0') {
      phoneNumberArray[0] = '62';
      phone = phoneNumberArray.join('');
    }

    if (phoneNumberArray[0] === '+') {
      phoneNumberArray[0] = '';
      phone = phoneNumberArray.join('');
    }

    const regex = new RegExp('-', 'g');
    phone = phone.replace(/ /g, '');
    phone = phone.replace(regex, '');

    return phone;
  } catch (error) {
    phoneNumber;
  }
}

// Untuk generate random string
// {length:number} -> Params length tipe data number
export function generateRandomString(length: number) {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function formatRupiah(angka, prefix) {
  if (!angka) return prefix == undefined ? 0 : 0 ? 'Rp. ' + 0 : '';

  const number_string = angka
    .toString()
    .replace(/[^,\d]/g, '')
    .toString();
  const split = number_string.split(',');
  const sisa = split[0].length % 3;
  let rupiah = split[0].substr(0, sisa);
  const ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if (ribuan) {
    const separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : rupiah ? 'Rp. ' + rupiah : '';
}

export function convertLocalDateToUTCDate(date, toUTC) {
  date = new Date(date);
  //Local time converted to UTC
  const localOffset = date.getTimezoneOffset() * 60000;
  const localTime = date.getTime();
  if (toUTC) {
    date = localTime + localOffset;
  } else {
    date = localTime - localOffset;
  }
  date = new Date(date);
  return date;
}

export async function convertArrayToStringWhere(wheres) {
  let dataReturn = ``;

  for await (const [index, where] of wheres.entries()) {
    if (index === 0) {
      dataReturn += `WHERE ${where}`;
    } else {
      dataReturn += ` AND ${where}`;
    }
  }

  return dataReturn;
}

export async function convertArrayToStringGroupBy(groups) {
  let dataReturn = ``;

  for await (const [index, group] of groups.entries()) {
    if (index === 0) {
      dataReturn += `GROUP BY ${group}`;
    } else {
      dataReturn += `, ${group}`;
    }
  }

  return dataReturn;
}

export async function convertArrayToStringOrderBy(orders) {
  let dataReturn = ``;

  for await (const [index, order] of orders.entries()) {
    if (index === 0) {
      dataReturn += `ORDER BY ${order}`;
    } else {
      dataReturn += `, ${order}`;
    }
  }

  return dataReturn;
}

export async function convertArrayToStringSelect(selects, endString = '') {
  let dataReturn = ``;

  for await (const [index, select] of selects.entries()) {
    if (index === 0) {
      dataReturn += `${select}`;
    } else {
      dataReturn += `, ${select}`;
    }

    if (index === selects.length - 1) {
      dataReturn += `${endString}`;
    }
  }

  return dataReturn;
}
