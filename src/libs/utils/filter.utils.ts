export class Filter {
  alreadyJoin: boolean;
  total: number;

  constructor(
    private query,
    private data,
    private searchFields,
    private tableName = '',
    private joinField = [],
  ) {
    this.query = query;

    this.alreadyJoin = false;
    this.total = 0;
  }

  generateSearch(search) {
    let querySearch = '';

    this.searchFields.forEach((field) => {
      querySearch += `${
        querySearch ? 'OR' : ''
      } LOWER(${field}) like LOWER(:search) `;
    });

    this.query.andWhere(querySearch, { search: `%${search}%` });
  }

  generateOrdering(ordering) {
    const splitOrdering = ordering.split('-');
    const field =
      splitOrdering.length > 1 ? splitOrdering[1] : splitOrdering[0];
    const sort = splitOrdering.length > 1 ? 'DESC' : 'ASC';

    const orderBy = {};

    if (this.tableName) {
      orderBy[`${this.tableName}.${this.snakeToCamel(field)}`] = sort;
    } else {
      orderBy[field] = sort;
    }

    this.query.orderBy(orderBy);
  }

  generatePagination(page, limit) {
    if (page) {
      this.query.skip(limit * (page - 1));
      this.query.take(limit);
    } else if (limit) {
      this.query.limit(limit);
    }
  }

  async joinTable() {
    if (!this.alreadyJoin) {
      for await (const field of this.joinField) {
        const fieldArr = field.split(',');

        this.query.leftJoinAndSelect(fieldArr[0], fieldArr[1]);
      }

      this.alreadyJoin = true;
    }
  }

  async generate(returnQuery = false, useGroup = false) {
    const { search, page, limit, ordering } = this.data;

    await this.joinTable();

    if (search) {
      await this.generateSearch(search);
    }

    if (ordering) {
      await this.generateOrdering(ordering);
    }

    if ((page || limit) && !useGroup) {
      await this.generatePagination(parseInt(page), parseInt(limit));
    }

    if (returnQuery) {
      return await this.query;
    } else {
      const data = await this.query.getMany();

      if (useGroup) this.total = data.length;

      return data;
    }
  }

  async count() {
    const { search } = this.data;

    await this.joinTable();

    if (search) {
      this.generateSearch(search);
    }

    if (this.total > 0) {
      return this.total;
    } else {
      return await this.query.getCount();
    }
  }

  async addSelects(fields) {
    for await (const field of fields) {
      await this.query.addSelect(field);
    }
  }

  async addGroupBy(fields) {
    for await (const field of fields) {
      await this.query.addGroupBy(field);
    }
  }

  async addHaving(have) {
    await this.query.having(have);
  }

  async addOrHaving(have) {
    await this.query.orHaving(have);
  }

  async addAndHaving(have) {
    await this.query.andHaving(have);
  }

  async addOrderBy(orderBy) {
    await this.query.orderBy(orderBy);
  }

  private snakeToCamel(str) {
    return str.replace(/([-_]\w)/g, (g) => g[1].toUpperCase());
  }
}
