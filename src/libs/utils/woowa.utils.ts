import { Setting } from 'src/settings/settings.entity';

export class Woowa {
  async send(payload: { phone_no: string; message: string; url?: string }) {
    const config = await Setting.findOne({ where: { key: 'WOOWA_KEY' } });

    const axios = require('axios');

    const response = await axios.post(
      `http://116.203.191.58/api/${
        payload['url'] ? 'send_image_url' : 'send_message'
      }`,
      {
        ...payload,
        key: config.value,
      },
    );
  }
}
