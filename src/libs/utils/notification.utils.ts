require('dotenv').config();
const PusherData = require('pusher');

export class Pusher {
  async send(channel, event, data) {
    const pusher = new PusherData({
      appId: process.env.PUSHER_APP_ID,
      key: process.env.PUSHER_KEY,
      secret: process.env.PUSHER_SECRET,
      cluster: process.env.PUSHER_CLUSTER,
      useTLS: true,
    });

    try {
      await pusher.trigger(channel, event, data);
    } catch (error) {}
  }

  async authentication(socketId, channel, presenceData) {
    const pusher = new PusherData({
      appId: process.env.PUSHER_APP_ID,
      key: process.env.PUSHER_KEY,
      secret: process.env.PUSHER_SECRET,
      cluster: process.env.PUSHER_CLUSTER,
      useTLS: true,
    });

    try {
      const data = await pusher.authenticate(socketId, channel, presenceData);
      return data;
    } catch (error) {}
  }
}
