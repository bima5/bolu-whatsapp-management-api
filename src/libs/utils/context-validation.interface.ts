import { ValidationArguments } from 'class-validator';

export interface ContextValidationArguments extends ValidationArguments {
  object: {
    context: {
      customerId: number;
      userId: number;
    };
  } & Object;
}
