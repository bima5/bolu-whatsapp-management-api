import { cloneDeep } from 'lodash';
import { format } from 'date-fns';
import localeId from 'date-fns/locale/id';
import { id, enUS } from 'date-fns/locale';
import * as moment from 'moment-timezone';
moment.locale('id');

export function convertDate(date, format) {
  return moment(date).tz('Asia/Jakarta').format(format);
}

export class DateHelper {
  getLocaleId() {
    const formatRelativeLocale = {
      lastWeek: "eeee 'pukul' p",
      yesterday: "'Kemarin pukul' p",
      today: "'Hari ini pukul' p",
      tomorrow: "'Besok pukul' p",
      nextWeek: "eeee 'pukul' p",
      other: 'P',
    };

    const monkeyLocale = Object.assign(cloneDeep(localeId), {
      formatRelative(token, _date, _baseDate, _options) {
        return formatRelativeLocale[token];
      },
    });
    return monkeyLocale;
  }

  getLocale() {
    return this.getLocaleId();
  }

  dateFormat(date, pattern, options = {}, en = false) {
    const defaultOptions = {
      // locale: this.getLocaleId(),
    };
    return format(new Date(date), pattern, {
      ...Object.assign({}, defaultOptions, options),
      locale: en ? enUS : id,
    });
  }

  getRangeDateFormat(start, end) {
    start = new Date(start);
    end = new Date(end);

    if (
      start.getMonth() === end.getMonth() &&
      start.getYear() === end.getYear()
    ) {
      return `${this.dateFormat(start, 'd')} - ${this.dateFormat(
        end,
        'd MMMM y',
      )}`;
    } else if (
      start.getMonth() === end.getMonth() ||
      start.getYear() === end.getYear()
    ) {
      return `${this.dateFormat(start, 'd MMMM')} - ${this.dateFormat(
        end,
        'd MMMM y',
      )}`;
    } else {
      return `${this.dateFormat(start, 'd MMMM y')} - ${this.dateFormat(
        end,
        'd MMMM y',
      )}`;
    }
  }

  convertTZ(date, tzString) {
    return new Date(
      (typeof date === 'string' ? new Date(date) : date).toLocaleString(
        'en-US',
        { timeZone: tzString },
      ),
    );
  }
}
