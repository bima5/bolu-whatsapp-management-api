import { upload } from './upload.utils';
import { fabric } from 'fabric';

export class Pdf {
  async convertCanvasToPdf(canvasTemplate) {
    const fabricCanvas = new fabric.StaticCanvas(null, {
      width: 715,
      height: 500,
    });
    const image = new upload();
    const fs = require('fs');

    fabricCanvas.loadFromJSON(JSON.parse(canvasTemplate), async (e) => {
      fabricCanvas.renderAll();

      const path = await image.uploadImage(
        fabricCanvas.toDataURL(),
        'certificate',
        'tes',
      );
    });
  }
}
