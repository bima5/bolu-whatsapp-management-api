import { DateHelper } from './date.utils';
import { upload } from './upload.utils';

export class Excel {
  convertFormat(data, format) {
    const date = new DateHelper();

    if (!data) return data;

    try {
      switch (format) {
        case 'date':
          const dataDate = typeof data === 'string' ? new Date(data) : data;
          return date.dateFormat(dataDate, 'd MMMM y - HH:mm:SS');

        default:
          return data;
      }
    } catch (error) {
      return data;
    }
  }

  async exportExcel(sheets, name, folder = 'excel') {
    // Require library
    var xl = require('excel4node');
    const path = `./assets/excel/${name}.xlsx`;

    // Create a new instance of a Workbook class
    var wb = new xl.Workbook();

    // Add Worksheets to the workbook
    for await (const sheet of sheets) {
      var ws = wb.addWorksheet(sheet.name);

      for await (const [index, header] of sheet.headers.entries()) {
        ws.cell(1, index + 1).string(header.label);
      }

      for await (const [indexRow, row] of sheet.rows.entries()) {
        for await (const [index, header] of sheet.headers.entries()) {
          ws.cell(indexRow + 2, index + 1).string(
            this.convertFormat(row[header.key], header.format),
          );
        }
      }
    }

    const util = require('util');
    wb.writeP = util.promisify(wb.write);

    try {
      const result = await wb.writeP(path);
    } catch (error) {}

    // await wb.write(path);

    const fs = require('fs');
    const contents = await fs.readFileSync(path, { encoding: 'base64' });
    const uploadFile = new upload();

    const url = await uploadFile.uploadImage(
      contents,
      folder,
      name,
      'xlsx',
      'files',
      true,
    );
    await uploadFile.removeFile(path);

    return url;
  }
}
