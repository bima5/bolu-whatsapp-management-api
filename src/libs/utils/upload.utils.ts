import { fabric } from 'fabric';
require('dotenv').config();
import { v4 as uuidv4 } from 'uuid';

const AWS = require('aws-sdk');
const aws = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

export class upload {
  async uploadImage(
    base64,
    directory,
    name,
    extensionDefault = '',
    type = 'images',
    useName = false,
  ) {
    let extension = extensionDefault;
    let base64Data = base64;

    if (base64.split(';base64,').length > 1) {
      base64Data = base64.split(';base64,')[1];
      if (extension === '') {
        extension = base64.split(';base64,')[0].split('data:image/')[1];
      }
    }

    const path = `${
      process.env.MODE !== 'production' ? process.env.MODE + '/' : ''
    }${type}/${directory}/${useName ? name : uuidv4()}.${extension}`;

    const buf = Buffer.from(base64Data, 'base64');
    const data = {
      Bucket: process.env.AWS_BUCKET,
      Key: path,
      Body: buf,
      ContentEncoding: 'base64',
      ContentType: `image/${extension}`,
    };

    const upload = await aws.upload(data).promise();
    // upload.Location;

    return `${process.env.AWS_CLOUDFRONT}${path}`;
  }

  async uploadVideo(
    base64,
    directory,
    name,
    extensionDefault = '',
    type = 'videos',
    useName = false,
  ) {
    let extension = extensionDefault;
    let base64Data = base64;

    if (base64.split(';base64,').length > 1) {
      base64Data = base64.split(';base64,')[1];
      if (extension === '') {
        extension = base64.split(';base64,')[0].split('data:video/')[1];
      }
    }

    const path = `${
      process.env.MODE !== 'production' ? process.env.MODE + '/' : ''
    }${type}/${directory}/${useName ? name : uuidv4()}.mp4`;

    const buf = Buffer.from(base64Data, 'base64');
    const data = {
      Bucket: process.env.AWS_BUCKET,
      Key: path,
      Body: buf,
      ContentEncoding: 'base64',
      ContentType: `video/${extension}`,
    };

    const upload = await aws.upload(data).promise();

    // upload.Location;

    return `${process.env.AWS_CLOUDFRONT}${path}`;
  }

  uploadFile(base64, directory, name) {
    const fs = require('fs');

    const data = base64.split(';base64,')[1];
    const extension = base64.split(';base64,')[0].split('data:text/')[1];
    const path = `${directory}/${name}.${extension}`;

    if (!fs.existsSync(`./assets/${directory}`)) {
      fs.mkdirSync(`./assets/${directory}`);
    }

    fs.writeFile(`./assets/${path}`, data, 'base64', function (err) {});

    return `./assets/${path}`;
  }

  async removeFile(path) {
    const fs = require('fs');

    try {
      fs.unlinkSync(`./public/${path}`);
    } catch (error) {}

    try {
      aws
        .deleteObject({
          Bucket: process.env.AWS_BUCKET,
          Key: path.split(process.env.AWS_CLOUDFRONT)[1],
        })
        .promise();
    } catch (error) {}
  }

  async canvasToImage(
    canvasTemplate,
    name,
    folder = 'certificate',
    useName = false,
  ) {
    const fabricCanvas = new fabric.StaticCanvas(null, {
      width: 850,
      height: 600,
    });

    const fonts = [
      'Montserrat-Bold',
      'Montserrat-Black',
      'Montserrat-Regular',
      'Montserrat-Thin',
      'Montserrat-Light',
      'Montserrat-Regular',
      'OpenSans-Light',
    ];

    for await (const font of fonts) {
      fabric.nodeCanvas.registerFont(`./assets/fonts/${font}.ttf`, {
        family: `${font}`,
      });
    }

    await fabricCanvas.loadFromJSON(JSON.parse(canvasTemplate));
    await fabricCanvas.renderAll();
    const path = await this.uploadImage(
      fabricCanvas.toDataURL({ format: 'jpeg', quality: 1, multiplier: 2 }),
      folder,
      name,
      '',
      'images',
      useName,
    );

    return path;
  }
}
