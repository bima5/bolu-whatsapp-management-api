async function generateSingleData(data, fields, raw = {}) {
  const response = raw;
  for await (const field of fields.split(',')) {
    try {
      const nesteds = field.split('.');

      if (nesteds.length > 1) {
        let isUndefined = false;

        if (response[nesteds[0]] === undefined) isUndefined = true;

        if (Array.isArray(data[nesteds[0]])) {
          if (isUndefined) response[nesteds[0]] = [];

          for await (const [index, item] of data[nesteds[0]].entries()) {
            const newData = await generateSingleData(
              item,
              nesteds.slice(1, 999).join('.'),
              response[nesteds[0]][index],
            );

            if (isUndefined) response[nesteds[0]].push(newData);
          }
        } else {
          if (isUndefined) response[nesteds[0]] = {};

          response[nesteds[0]] = await generateSingleData(
            data[nesteds[0]],
            nesteds.slice(1, 999).join('.'),
            response[nesteds[0]],
          );
        }
      }

      response[field] = data[field];
    } catch (error) {}
  }

  return response;
}

export async function formatResponse(
  data,
  fields,
  multiple = false,
): Promise<any> {
  let response;

  if (multiple) {
    response = [];

    for await (const item of data) {
      response.push(await generateSingleData(item, fields));
    }
  } else {
    response = {};

    response = await generateSingleData(data, fields);
  }

  return response;
}
