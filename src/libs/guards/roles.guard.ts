import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    let allow = false;
    const userRoles = user.roles.map((role) => role.name);

    if (typeof roles) {
      for await (const userRole of userRoles) {
        if (roles.indexOf(userRole) > -1) allow = true;
      }
    } else {
      allow = userRoles.indexOf(roles) > -1;
    }

    return allow;
  }
}
