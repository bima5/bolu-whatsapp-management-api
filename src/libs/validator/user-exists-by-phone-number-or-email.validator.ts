import { ExecutionContext, Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { REQUEST_CONTEXT_ID } from '@nestjs/core/router/request/request-constants';
import { InjectRepository } from '@nestjs/typeorm';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from 'src/users/users.entity';
import { UserRepository } from 'src/users/repositories/users.repository';
import { UsersService } from 'src/users/services/users.service';
import {
  Connection,
  Raw,
  Repository,
  UsingJoinColumnIsNotAllowedError,
} from 'typeorm';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';
import { convertToPhoneNumber } from '../utils/string-transform';

let message = '';

@ValidatorConstraint({ name: 'UserExistsByPhoneNumberOrEmail', async: true })
@Injectable()
export class UserExistsByPhoneNumberOrEmailRule
  implements ValidatorConstraintInterface {
  constructor(protected readonly userService: UsersService) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    const isEmail = val.split('@').length > 1;
    const isPhone = val.split('08').length > 1 || val.split('62').length > 1;
    let exists = false;
    let user = null;

    let value = val;
    let valueArray = value.split('');
    if (valueArray[0] === '0' && isPhone) {
      valueArray[0] = '62';
      value = valueArray.join('');
    }

    if (isEmail) {
      user = await User.findOne({
        where: {
          email: Raw((alias) => `LOWER(${alias}) = LOWER(:value)`, {
            value,
          }),
        },
      });
      message = 'Pengguna dengan email tersebut tidak ditemukan';
    } else if (isPhone) {
      user = await User.findOne({
        where: { phoneNumber: convertToPhoneNumber(val) },
      });
      message = 'Pengguna dengan nomer telpon tersebut tidak ditemukan';
    } else {
      user = await User.findOne({
        where: {
          username: Raw((alias) => `LOWER(${alias}) = LOWER(:value)`, {
            value,
          }),
        },
      });
      message = 'Pengguna dengan username tersebut tidak ditemukan';
    }

    if (!user) {
      return false;
    } else {
      return true;
    }
  }

  public defaultMessage(args: ValidationArguments): string {
    return message;
  }
}

export function UserExistsByPhoneNumberOrEmail(
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserExistsByPhoneNumberOrEmail',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserExistsByPhoneNumberOrEmailRule,
    });
  };
}
