import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UsersService } from 'src/users/services/users.service';
import { User } from 'src/users/users.entity';
import { Not } from 'typeorm';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';
import { convertToPhoneNumber } from '../utils/string-transform';

@ValidatorConstraint({ name: 'UniquePhoneNumber', async: true })
@Injectable()
export class UniquePhoneNumberRule implements ValidatorConstraintInterface {
  constructor(protected readonly userService: UsersService) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    const where: any = {
      phoneNumber: convertToPhoneNumber(val),
    };

    if (args.object['email']) {
      where.email = Not(args.object['email']);
    }

    const exists = await User.findOne({
      where,
    });

    if (!exists) {
      return true;
    } else {
      return false;
    }
  }

  public defaultMessage(args: ValidationArguments): string {
    return `Phone number has been used`;
  }
}

export function UniquePhoneNumber(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UniquePhoneNumber',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UniquePhoneNumberRule,
    });
  };
}
