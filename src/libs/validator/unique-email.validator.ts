import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UsersService } from 'src/users/services/users.service';
import { User } from 'src/users/users.entity';
import { Raw } from 'typeorm';
import { Not } from 'typeorm';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';
import { convertToPhoneNumber } from '../utils/string-transform';

@ValidatorConstraint({ name: 'UniqueEmail', async: true })
@Injectable()
export class UniqueEmailRule implements ValidatorConstraintInterface {
  constructor(protected readonly userService: UsersService) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    if (val === '') return true;

    const where: any = {
      email: Raw((alias) => `LOWER(${alias}) = LOWER(:value)`, {
        value: val,
      }),
    };

    if (args.object['phoneNumber']) {
      where.phoneNumber = Not(convertToPhoneNumber(args.object['phoneNumber']));
    }

    const exists = await User.findOne({
      where,
    });

    if (!exists) {
      return true;
    } else {
      return false;
    }
  }

  public defaultMessage(args: ValidationArguments): string {
    return `Email has been used`;
  }
}

export function UniqueEmail(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UniqueEmail',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UniqueEmailRule,
    });
  };
}
