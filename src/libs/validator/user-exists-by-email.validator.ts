import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UsersService } from 'src/users/services/users.service';
import { User } from 'src/users/users.entity';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';

let message = '';

@ValidatorConstraint({ name: 'UserExistsByEmail', async: true })
@Injectable()
export class UserExistsByEmailRule implements ValidatorConstraintInterface {
  constructor(protected readonly userService: UsersService) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    const isEmail = val.split('@').length > 1;
    let user = null;

    user = await User.findOne({ where: { email: val } });
    message = 'Pengguna dengan email tersebut tidak ditemukan';

    if (!user) {
      return false;
    } else {
      return true;
    }
  }

  public defaultMessage(args: ValidationArguments): string {
    return message;
  }
}

export function UserExistsByEmail(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserExistsByEmail',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserExistsByEmailRule,
    });
  };
}
