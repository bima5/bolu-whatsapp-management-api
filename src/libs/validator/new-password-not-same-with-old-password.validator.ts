import { ExecutionContext, Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { REQUEST_CONTEXT_ID } from '@nestjs/core/router/request/request-constants';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UserRepository } from 'src/users/repositories/users.repository';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';

@ValidatorConstraint({ name: 'NewPasswordNotSameWithOldPassword', async: true })
@Injectable()
export class NewPasswordNotSameWithOldPasswordRule
  implements ValidatorConstraintInterface {
  constructor(private userRepository: UserRepository) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    try {
      if (await args?.object._requestContext.user.validatedPassword(val)) {
        return false;
      }
    } catch (error) {
      return false;
    }
    return true;
  }

  public defaultMessage(args: ValidationArguments): string {
    return `It can't be the same as the old password`;
  }
}

export function NewPasswordNotSameWithOldPassword(
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'NewPasswordNotSameWithOldPassword',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: NewPasswordNotSameWithOldPasswordRule,
    });
  };
}
