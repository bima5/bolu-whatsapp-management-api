import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from 'src/users/users.entity';
import { UsersService } from 'src/users/services/users.service';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';

@ValidatorConstraint({ name: 'UniqueUsername', async: true })
@Injectable()
export class UniqueUsernameRule implements ValidatorConstraintInterface {
  constructor(protected readonly userService: UsersService) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    const exists = await User.findOne({ username: val });

    if (!exists) {
      return true;
    } else {
      return false;
    }
  }

  public defaultMessage(args: ValidationArguments): string {
    return `Username has been used`;
  }
}

export function UniqueUsername(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UniqueUsername',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UniqueUsernameRule,
    });
  };
}
