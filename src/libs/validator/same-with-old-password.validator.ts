import { ExecutionContext, Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { REQUEST_CONTEXT_ID } from '@nestjs/core/router/request/request-constants';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UserRepository } from 'src/users/repositories/users.repository';
import { ExtendedValidationArguments } from '../utils/extended-validation-arguments';

@ValidatorConstraint({ name: 'SameWithOldPassword', async: true })
@Injectable()
export class SameWithOldPasswordRule implements ValidatorConstraintInterface {
  constructor(private userRepository: UserRepository) {}

  public async validate(
    val: any,
    args?: ExtendedValidationArguments,
  ): Promise<boolean> {
    try {
      if (!(await args?.object._requestContext.user.validatedPassword(val))) {
        return false;
      }
    } catch (error) {
      return false;
    }
    return true;
  }

  public defaultMessage(args: ValidationArguments): string {
    return `Passwords are not the same as old passwords`;
  }
}

export function SameWithOldPassword(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'SameWithOldPassword',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: SameWithOldPasswordRule,
    });
  };
}
