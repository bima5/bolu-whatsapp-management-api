import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../users.entity';

export enum UserProfileGender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
}

@Entity()
export class UserProfile extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => User, (user: User) => user.profile, { onDelete: 'SET NULL' })
  user: User;

  @Column({ nullable: true })
  instagram: string;

  @Column({ nullable: true })
  tiktok: string;

  @Column({ nullable: true })
  facebook: string;

  @Column({ nullable: true })
  typeSelling: string;

  @Column({ nullable: true })
  averageProfitPerMonth: string;

  @Column({ nullable: true })
  gender: UserProfileGender;

  @Column({ nullable: true })
  dateBirth: Date;

  @Column({ nullable: true })
  yearOfBirth: Date;

  @Column({ default: false })
  isPaidMember: boolean;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
