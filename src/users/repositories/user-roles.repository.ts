import { Filter } from 'src/libs/utils/filter.utils';
import { EntityRepository, Repository } from 'typeorm';
import { CreateUserRoleDTO } from '../dto/create-user-role.dto';
import { GetUserRoleFilterDTO } from '../dto/get-user-role-filter.dto';
import { UserRole } from '../users.entity';

@EntityRepository(UserRole)
export class UserRoleRepository extends Repository<UserRole> {
  async fetch(
    filterDto: GetUserRoleFilterDTO,
  ): Promise<{ total: number; data: UserRole[] }> {
    const query = this.createQueryBuilder('user_roles');
    query.loadRelationCountAndMap('user_roles.users', 'user_roles.users');

    const queryResult = new Filter(
      query,
      filterDto,
      ['user_roles.name'],
      'user_roles',
    );

    const total = await queryResult.count();
    const resultQuery = await queryResult.generate(true);

    const data = await resultQuery.getMany();

    return { total, data };
  }

  // Untuk membuat user role baru
  async store(payload: CreateUserRoleDTO): Promise<UserRole> {
    const userRole = await UserRole.create(payload);

    return await userRole.save();
  }
}
