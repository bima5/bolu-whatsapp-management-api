import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UniquePhoneNumber } from 'src/libs/validator/unique-phone-number.validator';
import { UserProfileGender } from '../entities/user-profiles.entity';

export class ChangeProfileUserDTO {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsString()
  @UniquePhoneNumber()
  phoneNumber?: string;

  @IsNotEmpty()
  email: string;

  @IsOptional()
  profilePicture?: string;

  @IsOptional()
  @IsString()
  instagram?: string;

  @IsOptional()
  @IsString()
  categoryId?: string;

  @IsOptional()
  @IsString()
  provinceId?: string;

  @IsOptional()
  @IsString()
  cityId?: string;

  @IsOptional()
  @IsString()
  subdistrictId?: string;

  @IsOptional()
  dateBirth?: Date;

  @IsOptional()
  yearOfBirth?: string;

  @IsOptional()
  gender?: UserProfileGender;
}
