import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { UserRoles } from '../users.entity';

export class UpdateUserDto {
  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsOptional()
  @IsString()
  lastName: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @IsMobilePhone('id-ID')
  phoneNumber: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(4)
  @MaxLength(16)
  username: string;

  @IsOptional()
  @MinLength(8)
  @MaxLength(16)
  password: string;

  @IsOptional()
  roleIds: [];

  @IsOptional()
  profilePicture: string;

  @IsOptional()
  isActive: boolean;

  @IsOptional()
  utms: string;
}
