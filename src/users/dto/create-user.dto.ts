import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { UniqueEmail } from 'src/libs/validator/unique-email.validator';
import { UniquePhoneNumber } from 'src/libs/validator/unique-phone-number.validator';
import { UniqueUsername } from 'src/libs/validator/unique-username.validator';
import { UserRoles, UserTypeAudience } from '../users.entity';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsOptional()
  @IsString()
  lastName: string;

  @IsOptional()
  @IsString()
  @UniqueEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @IsMobilePhone('id-ID')
  @UniquePhoneNumber()
  phoneNumber: string;

  @IsOptional()
  @IsString()
  @UniqueUsername()
  username: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(16)
  password: string;

  @IsOptional()
  roleIds?: string[];

  @IsOptional()
  profilePicture: string;

  @IsOptional()
  isActive: boolean;

  @IsOptional()
  utms: string;

  @IsOptional()
  dateFirstJoined?: Date;

  @IsOptional()
  type?: UserTypeAudience;

  @IsOptional()
  ltv?: number;

  @IsOptional()
  aff?: number;

  @IsOptional()
  createdAt?: Date;
}
