import { IsNotEmpty, IsOptional } from 'class-validator';

export class FetchUserCategoriesDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;
}

export class CreateUserCategoryDTO {
  @IsNotEmpty()
  name: string;
}

export class DetailUserCategoryDTO {
  id: string;
  fields?: string;
}
