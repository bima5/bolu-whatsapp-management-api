export class ParamGetTotalMemberAlreadyLoginDTO {
  startDate?: Date;
  endDate?: Date;
  courseIds?: string;
  batchIds?: string;
}
