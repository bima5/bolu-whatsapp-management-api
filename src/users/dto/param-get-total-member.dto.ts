export class ParamGetTotalMemberDTO {
  startDate?: Date;
  endDate?: Date;
  courseIds?: string;
  batchIds?: string;
}
