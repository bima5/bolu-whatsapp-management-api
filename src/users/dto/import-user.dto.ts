import { IsNotEmpty } from 'class-validator';

export class ImportUserDTO {
  @IsNotEmpty()
  file: string;
}
