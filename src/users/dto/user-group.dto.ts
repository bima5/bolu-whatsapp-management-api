import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class FetchUserGroupFilterDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;
}

export class CreateUserGroupDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  link: string;
}
