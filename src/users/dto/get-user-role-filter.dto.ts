import { IsOptional } from 'class-validator';

export class GetUserRoleFilterDTO {
  @IsOptional()
  ordering: string;

  @IsOptional()
  search: string;

  @IsOptional()
  page: number;

  @IsOptional()
  limit: number;
}
