import { IsOptional } from 'class-validator';
import { UserProfileGender } from '../entities/user-profiles.entity';

export class CreateOrUpdateUserProfileDTO {
  instagram?: string;
  categoryId?: string;
  provinceId?: string;
  cityId?: string;
  subdistrictId?: string;
  dateBirth?: Date;
  gender?: UserProfileGender;
  referalCode?: string;
}

export class RegisterUserDTO {
  username?: string;
  name: string;
  password: string;
  email: string;
  phoneNumber: string;
  profile?: CreateOrUpdateUserProfileDTO;
  utm?: string;
}

export class FilterUserOtpDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;
}
