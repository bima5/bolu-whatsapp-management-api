import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { NewPasswordNotSameWithOldPassword } from 'src/libs/validator/new-password-not-same-with-old-password.validator';
import { SameWithOldPassword } from 'src/libs/validator/same-with-old-password.validator';

export class ChangePasswordDTO {
  @IsNotEmpty()
  @IsString()
  @SameWithOldPassword()
  oldPassword: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(16)
  @NewPasswordNotSameWithOldPassword()
  newPassword: string;
}
