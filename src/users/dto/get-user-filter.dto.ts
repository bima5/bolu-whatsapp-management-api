import { IsOptional } from 'class-validator';

export class GetUserFilterDto {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  roleIds?: string;

  @IsOptional()
  roles?: string;

  @IsOptional()
  utms?: string;

  @IsOptional()
  groupId?: string;

  @IsOptional()
  courseIds?: string;

  @IsOptional()
  startCreatedAt?: Date;

  @IsOptional()
  endCreatedAt?: Date;
}
