import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserRoleDTO } from '../dto/create-user-role.dto';
import { GetUserRoleFilterDTO } from '../dto/get-user-role-filter.dto';
import { UserRoleRepository } from '../repositories/user-roles.repository';
import { UserRole } from '../users.entity';

@Injectable()
export class UserRoleService {
  constructor(
    @InjectRepository(UserRoleRepository)
    private readonly userRoleRepository: UserRoleRepository,
  ) {}

  // Untuk mendapatkan list role
  async fetch(
    filterDto: GetUserRoleFilterDTO,
  ): Promise<{ total: number; data: UserRole[] }> {
    return await this.userRoleRepository.fetch(filterDto);
  }

  async store(payload: CreateUserRoleDTO): Promise<UserRole> {
    return await this.userRoleRepository.store(payload);
  }

  // Untuk mendapatkan role berdasarkan nama
  // Params name -> type data string
  // Return Promise dari data User Role
  async findByName(name: string): Promise<UserRole> {
    return await this.userRoleRepository.findOne({ where: { name } });
  }
}
