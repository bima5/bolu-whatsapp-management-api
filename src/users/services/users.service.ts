import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { upload } from 'src/libs/utils/upload.utils';
import { CreateUserDto } from '../dto/create-user.dto';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User, UserRole } from '../users.entity';
import { UserRepository } from '../repositories/users.repository';
import {
  convertToPhoneNumber,
  generateRandomString,
} from 'src/libs/utils/string-transform';
import { I18nRequestScopeService } from 'nestjs-i18n';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    public userRepository: UserRepository,

    private readonly i18n: I18nRequestScopeService,
  ) {}

  // For create new user
  async store(storeDto: CreateUserDto): Promise<User> {
    const {
      firstName,
      lastName,
      email,
      username,
      password,
      roleIds,
      profilePicture,
      phoneNumber,
      utms,
      isActive,
      ltv,
      dateFirstJoined,
      type,
      aff,
      createdAt,
    } = storeDto;

    const userPhoneExists = await User.findOne({ phoneNumber });

    if (userPhoneExists) {
      throw new InternalServerErrorException(
        await this.i18n.translate('message.errorAlreadyExists', {
          args: { name: 'User', field: 'nomer telpon', value: phoneNumber },
        }),
      );
    }

    const user = new User();
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.isActive = isActive;
    user.ltv = ltv;
    if (dateFirstJoined) user.dateFirstJoined = dateFirstJoined;
    user.type = type;
    user.username = username || generateRandomString(8);

    if (createdAt) user.createdAt = createdAt;

    user.phoneNumber = convertToPhoneNumber(phoneNumber);

    user.salt = await bcrypt.genSalt();
    user.password = await this.userRepository.hashPassword(password, user.salt);

    if (roleIds) {
      user.roles = [];

      for await (const roleId of roleIds) {
        user.roles.push(await UserRole.findOne(roleId));
      }
    } else {
      user.roles = [await UserRole.findOne({ name: 'MEMBER' })];
    }

    if (profilePicture) {
      if (profilePicture.split('base64').length > 1) {
        const imagePath = await new upload().uploadImage(
          profilePicture,
          'user',
          username,
        );
        user.profilePicture = imagePath;
      }
    }

    await user.save();

    delete user.salt;
    delete user.password;

    return user;
  }

  // Get users
  async fetch(
    filterDto: GetUserFilterDto,
  ): Promise<{ total: number; data: User[] }> {
    return await this.userRepository.fetch(filterDto);
  }

  // Get user by id
  async getUserById(id: string, fullDetail?: boolean): Promise<User> {
    let relations = [
      'utms',
      'utmsAfterRegister',
      'roles',
      'profile',
      'profile.category',
      'profile.province',
      'profile.city',
      'profile.subdistrict',
      'onboardingAnswers',
      'onboardingAnswers.options',
      'onboardingAnswers.onboarding',
    ];

    if (fullDetail) {
      relations = ['roles'];
    }

    const user: any = await this.userRepository.findOne({
      where: { id },
      relations,
    });

    delete user.password;
    delete user.salt;
    // delete user.currentHashedRefreshToken;

    if (!user) {
      throw new NotFoundException(`User with id "${id}" not found`);
    }

    return user;
  }

  // Mendapatkan data user berdasarkan nama role
  async getUserByRoleName(role: string): Promise<User[]> {
    return await this.userRepository.find({ where: { role } });
  }

  // Get user by username
  async getUserByUsername(username: string): Promise<User> {
    const user = await this.userRepository
      .createQueryBuilder('user')
      .where('LOWER(username) = LOWER(:value)', { value: username })
      .getOne();

    return user;
  }

  // Get user by phoneNumber
  async getUserByPhoneNumber(phoneNumber: string): Promise<User> {
    const user = await this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.onboardingAnswers', 'onboardingAnswers')
      .leftJoinAndSelect('user.group', 'group')
      .where('LOWER(phone_number) = LOWER(:value)', {
        value: convertToPhoneNumber(phoneNumber),
      })
      .getOne();

    return user;
  }

  // Get user by email
  async getUserByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({ email });

    return user;
  }

  // Delete user by id
  async deleteUserById(id): Promise<void> {
    return await this.userRepository.deleteUserById(id);
  }

  // Update user by id
  async updateById(id, updateUserDto: UpdateUserDto): Promise<User> {
    return await this.userRepository.updateById(id, updateUserDto);
  }
}
