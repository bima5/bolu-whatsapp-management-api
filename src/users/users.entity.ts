import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserProfile } from './entities/user-profiles.entity';
import { WhatsappBlasterQueue } from 'src/whatsapp-bots/entities/whatsapp-blaster-queue.entity';
import { WhatsappBlasterUserAcquisition } from 'src/whatsapp-bots/entities/whatsapp-blaster-user-acquisition.entity';

export enum UserRoles {
  ADMIN = 'ADMIN',
  MEMBER = 'MEMBER',
  TEACHER = 'TEACHER',
  MENTOR = 'MENTOR',
  FASILITATOR = 'FASILITATOR',
}

export enum UserTypeAudience {
  RETAIL = 'RETAIL',
  PARTNERSHIP = 'PARTNERSHIP',
  BUSINESS_DEVELOPMENT = 'BUSINESS_DEVELOPMENT',
}

@Entity()
export class UserRole extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @ManyToMany(() => User, (user: User) => user.roles)
  users: User[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true, comment: 'Untuk identifier id user lama' })
  oldUserId: string;

  @Column({ name: 'first_name', nullable: true })
  firstName: string;

  @Column({ name: 'last_name', nullable: true })
  lastName: string;

  @Column({ nullable: true })
  email: string;

  @Column({ unique: true, nullable: true, name: 'phone_number' })
  phoneNumber: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({ default: UserTypeAudience.RETAIL })
  type: UserTypeAudience;

  @Column()
  salt: string;

  @Column({ nullable: true })
  currentHashedRefreshToken?: string;

  @Column({ default: 'MEMBER' })
  role: UserRoles;

  @Column({ default: 0 })
  ltv: number;

  @Column({ default: false })
  isOldMember: boolean;

  @Column({ nullable: true })
  dateFirstJoined: Date;

  @Column({ default: 0 })
  point: number;

  @Column({ nullable: true, name: 'profile_picture' })
  profilePicture: string;

  @Column({ default: false, name: 'is_active' })
  isActive: boolean;

  @Column({ default: false, name: 'is_blocked' })
  isBlocked: boolean;

  @Column({ default: false, name: 'is_email_verified' })
  isEmailVerified: boolean;

  @Column({ nullable: true, name: 'first_login_at' })
  firstLoginAt: Date;

  @Column({ nullable: true, name: 'last_login_at' })
  lastLoginAt: Date;

  @Column({ nullable: true })
  codeReference: string;

  @Column({ nullable: true, type: 'timestamptz' })
  verifiedAt: Date;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;

  @ManyToMany(() => UserRole, (userRole: UserRole) => userRole.users)
  @JoinTable()
  roles: UserRole[];

  @OneToOne(() => UserProfile, (userProfile: UserProfile) => userProfile.user, {
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  profile: UserProfile;

  // Online Status
  @Column({ default: false })
  isOnline?: boolean;

  @Column({ nullable: true, type: 'timestamptz' })
  lastOnlineAt?: Date;

  @OneToMany(
    () => WhatsappBlasterQueue,
    (whatsappBlasterQueue: WhatsappBlasterQueue) => whatsappBlasterQueue.user,
  )
  blasterQueues: WhatsappBlasterQueue[];

  @OneToMany(
    () => WhatsappBlasterUserAcquisition,
    (whatsappBlasterUserAcquisition: WhatsappBlasterUserAcquisition) =>
      whatsappBlasterUserAcquisition.user,
  )
  blasterAcquisitions: WhatsappBlasterUserAcquisition;

  async validatedPassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);

    return hash === this.password;
  }
}
