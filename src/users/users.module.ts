import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './repositories/users.repository';
import { UsersService } from './services/users.service';
import { UsersController } from './controllers/users.controller';
import { UniqueUsernameRule } from 'src/libs/validator/unique-username.validator';
import { UserExistsByPhoneNumberOrEmailRule } from 'src/libs/validator/user-exists-by-phone-number-or-email.validator';
import { UserExistsByEmailRule } from 'src/libs/validator/user-exists-by-email.validator';

// User History Point
import { UserRoleRepository } from './repositories/user-roles.repository';
import { UserRoleService } from './services/user-role.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository, UserRoleRepository])],
  providers: [
    UniqueUsernameRule,
    // UniquePhoneNumberRule,
    // UniqueEmailRule,
    UserExistsByPhoneNumberOrEmailRule,
    UserExistsByEmailRule,
    UsersService,
    UserRoleService,
  ],
  controllers: [UsersController],
  exports: [UsersService, UserRoleService],
})
export class UsersModule {}
