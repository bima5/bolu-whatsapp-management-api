import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { UsersService } from '../services/users.service';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/libs/decorators/roles.decorator';
import { RolesGuard } from 'src/libs/guards/roles.guard';
import { I18nValidationPipe } from 'src/libs/pipes/validation.pipe';
import { I18nRequestScopeService } from 'nestjs-i18n';
import { formatResponse } from 'src/libs/utils/response-format.utils';

@Controller('users')
export class UsersController {
  constructor(
    private userService: UsersService,
    private readonly i18n: I18nRequestScopeService,
  ) {}

  @Post()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  @UsePipes(I18nValidationPipe)
  async store(@Body() storeDto: CreateUserDto) {
    const result = await this.userService.store({
      ...storeDto,
      isActive: true,
    });

    return {
      message: await this.i18n.translate('response.user.create'),
      result,
    };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN', 'FASILITATOR', 'MENTOR'])
  async fetch(@Query() filterDto: GetUserFilterDto) {
    const result = await this.userService.fetch(filterDto);

    return { message: await this.i18n.translate('response.user.list'), result };
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getUser(@Param('id') id: string) {
    const result = await this.userService.getUserById(id);

    return {
      message: await this.i18n.translate('response.user.detail'),
      result,
    };
  }

  @Get('/:id/detail')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN', 'FASILITATOR', 'MENTOR'])
  async getFullDetailUser(@Param('id') id: string) {
    const result = await this.userService.getUserById(id, true);

    return {
      message: await this.i18n.translate('response.user.detail'),
      result,
    };
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN', 'FASILITATOR'])
  @UsePipes(I18nValidationPipe)
  async updateById(
    @Param('id') id: string,
    @Body() updateUserDTO: UpdateUserDto,
  ) {
    const result = await this.userService.updateById(id, updateUserDTO);

    return {
      message: await this.i18n.translate('response.user.update'),
      result,
    };
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async deleteUserById(@Param('id') id: string) {
    const result = await this.userService.deleteUserById(id);

    return {
      message: await this.i18n.translate('response.user.delete'),
      result,
    };
  }

  @Get('/phone-number/:phoneNumber')
  async getUserByPhoneNUmber(@Param('phoneNumber') phoneNumber: string) {
    const user: any = await this.userService.getUserByPhoneNumber(phoneNumber);

    if (!user) throw new NotFoundException('User not found');

    user.alreadyOnboarding = user.onboardingAnswers.length > 0;
    user.group = user.group ? user.group.link : '';

    return {
      message: 'success get data user',
      result: await formatResponse(
        user,
        'id,firstName,lastName,phoneNumber,alreadyOnboarding,group,email',
        false,
      ),
    };
  }
}
