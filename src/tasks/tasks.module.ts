import { Module } from '@nestjs/common';
import { SettingsModule } from 'src/settings/settings.module';
import { WhatsappBotsModule } from 'src/whatsapp-bots/whatsapp-bots.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [SettingsModule, WhatsappBotsModule, WhatsappBotsModule],
  providers: [TasksService],
})
export class TasksModule {}
