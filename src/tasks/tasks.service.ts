import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import {
  whatsappBotGetMessageStatus,
  whatsappBotSendMessage,
  whatsappBotSendMessageBlaster,
} from 'src/libs/utils/bot-whatsapp.utils';
import { Setting } from 'src/settings/settings.entity';
import { WhatsappBlasterQueueHistoryStatus } from 'src/whatsapp-bots/entities/whatsapp-blaster-queue-history-status.entity';
import {
  WhatsappBlasterQueue,
  WhatsappBlasterQueueStatus,
} from 'src/whatsapp-bots/entities/whatsapp-blaster-queue.entity';
import {
  WhatsappBlaster,
  WhatsappBlasterStatus,
} from 'src/whatsapp-bots/entities/whatsapp-blaster.entity';
import {
  WhatsappNumberHost,
  WhatsappNumberHostStatus,
  WhatsappNumberHostType,
} from 'src/whatsapp-bots/entities/whatsapp-number-host.entity';
import { getConnection, In, IsNull, MoreThanOrEqual, Not } from 'typeorm';
import * as moment from 'moment-timezone';
import {
  WhatsappNumberHostActivity,
  WhatsappNumberHostActivityType,
} from 'src/whatsapp-bots/entities/whatsapp-number-host-activity.entity';
import * as _ from 'lodash';

@Injectable()
export class TasksService {
  constructor() {}

  // Check blaster message status
  @Cron('0 0 22 * * *', {
    timeZone: 'Asia/Jakarta',
  })
  async checkBlasterMessageStatus() {
    if (parseInt(process.env.NODE_APP_INSTANCE) === 0) {
      const queues = await WhatsappBlasterQueue.find({
        where: {
          status: WhatsappBlasterQueueStatus.DONE,
          updatedAt: MoreThanOrEqual(
            new Date(moment().subtract(7, 'd').format('YYYY-MM-DD')),
          ),
          messageWhatsappId: Not(IsNull()),
          seenAt: IsNull(),
        },
        relations: ['host'],
      });

      if (queues.length > 0) {
        for await (const queue of queues) {
          await whatsappBotGetMessageStatus(
            `${queue.phoneNumber}@c.us`,
            queue.messageWhatsappId,
            queue.host,
            queue.id,
          );
        }
      }
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async deepTalkBot() {
    if (parseInt(process.env.NODE_APP_INSTANCE) === 0) {
      const hosts = await WhatsappNumberHost.find({
        where: {
          status: WhatsappNumberHostStatus.CONNECTED,
        },
      });

      if (hosts.length > 0) {
        for await (const host of hosts) {
          const lastActivity = await WhatsappNumberHostActivity.findOne({
            where: { host },
            order: { createdAt: 'DESC' },
          });
          let skip = false;

          if (lastActivity) {
            if (
              moment
                .duration(
                  moment(new Date())
                    .tz('Asia/Jakarta')
                    .diff(moment(lastActivity.createdAt).tz('Asia/Jakarta')),
                )
                .asMinutes() < 1
            )
              skip = true;
          }

          if (!skip) {
            const randomHost = await getConnection()
              .createQueryBuilder(WhatsappNumberHost, 'whatsapp_number_host')
              .andWhere('id != :id', { id: host['id'] })
              .andWhere("number != '-'")
              .orderBy('RANDOM()')
              .getOne();

            if (randomHost.number.trim()) {
              const faker = require('faker');

              whatsappBotSendMessage(
                `${randomHost.number}@c.us`,
                {
                  message: faker.lorem.text(),
                  typeActivity:
                    WhatsappNumberHostActivityType.SEND_MESSAGE_TO_ANOTHER_BOT,
                },
                host,
              );
            }
          }
        }
      }
    }
  }

  // Scheduler Blaster
  @Cron(
    '0,30 1,2,4,5,7,8,10,11,13,14,16,17,19,20,22,23,25,26,28,29,31,32,34,35,37,38,40,41,43,44,46,47,49,50,52,53,55,56,58,59 6-21 * * *',
    {
      timeZone: 'Asia/Jakarta',
    },
  )
  async sendBlaster() {
    if (parseInt(process.env.NODE_APP_INSTANCE) === 0) {
      moment.locale('id');
      const faker = require('faker');

      let blaster = null;
      const blasters = await WhatsappBlaster.find({
        where: {
          isFinished: false,
          status: WhatsappBlasterStatus.PUBLISHED,
          isPaused: false,
        },
        order: { createdAt: 'ASC' },
      });

      for await (const data of blasters) {
        if (!blaster) {
          blaster = data;

          if (data.startAt) {
            if (
              moment(data.startAt)
                .tz('Asia/Jakarta')
                .diff(moment(new Date()).tz('Asia/Jakarta')) >= 0
            )
              blaster = null;
          }

          if (data.endAt) {
            if (
              moment(data.endAt)
                .tz('Asia/Jakarta')
                .diff(moment(new Date()).tz('Asia/Jakarta')) <= 0
            )
              blaster = null;
          }
        }
      }

      if (blaster) {
        const botAvailable = await WhatsappNumberHost.find({
          where: {
            type: WhatsappNumberHostType.BROADCAST_PROMO,
            isActive: true,
            status: WhatsappNumberHostStatus.CONNECTED,
          },
        });

        if (botAvailable.length > 0) {
          const queues = await WhatsappBlasterQueue.find({
            where: {
              blaster,
              status: WhatsappBlasterQueueStatus.PENDING,
              phoneNumber: Not(In(['#VALUE!'])),
            },
            order: { createdAt: 'ASC' },
            take: botAvailable.length,
          });

          let countingSend = 0;

          if (queues.length > 0) {
            for await (const [index, queue] of queues.entries()) {
              // Kirim whatsapp
              await whatsappBotSendMessageBlaster(
                `${queue.phoneNumber}@c.us`,
                {
                  message: queue.text,
                  id: queue.id,
                  image: queue.image,
                },
                botAvailable[index],
              );

              queue.host = botAvailable[index];
              queue.status = WhatsappBlasterQueueStatus.PROGRESS;
              await queue.save();

              blaster.totalQueueDone++;
              await blaster.save();

              const queueHistory = new WhatsappBlasterQueueHistoryStatus();
              queueHistory.name = queue.status;
              queueHistory.blasterQueue = queue;
              await queueHistory.save();

              try {
                const randomHost = await getConnection()
                  .createQueryBuilder(
                    WhatsappNumberHost,
                    'whatsapp_number_host',
                  )
                  .andWhere('id != :id', { id: botAvailable[index]['id'] })
                  .orderBy('RANDOM()')
                  .getOne();

                whatsappBotSendMessage(
                  `${botAvailable[index]['number']}@c.us`,
                  {
                    message: faker.lorem.text(),
                    typeActivity:
                      WhatsappNumberHostActivityType.SEND_MESSAGE_TO_ANOTHER_BOT,
                  },
                  randomHost,
                );
              } catch (error) {}

              countingSend++;
            }

            try {
              if (countingSend < botAvailable.length) {
                const nextBlaster = await WhatsappBlaster.findOne({
                  where: {
                    isFinished: false,
                    status: WhatsappBlasterStatus.PUBLISHED,
                    id: Not(blaster.id),
                  },
                  order: { createdAt: 'ASC' },
                });

                if (nextBlaster) {
                  const nextBlasterQueues = await WhatsappBlasterQueue.find({
                    where: {
                      blaster: nextBlaster,
                      status: WhatsappBlasterQueueStatus.PENDING,
                    },
                    order: { createdAt: 'DESC' },
                    take: botAvailable.length - countingSend,
                  });

                  for await (const queue of nextBlasterQueues) {
                    // Kirim whatsapp
                    await whatsappBotSendMessageBlaster(
                      `${queue.phoneNumber}@c.us`,
                      {
                        message: queue.text,
                        id: queue.id,
                        image: queue.image,
                      },
                      botAvailable[countingSend],
                    );

                    queue.host = botAvailable[countingSend];
                    queue.status = WhatsappBlasterQueueStatus.PROGRESS;
                    await queue.save();

                    blaster.totalQueueDone++;
                    await blaster.save();

                    const queueHistory = new WhatsappBlasterQueueHistoryStatus();
                    queueHistory.name = queue.status;
                    queueHistory.blasterQueue = queue;
                    await queueHistory.save();

                    try {
                      const randomHost = await getConnection()
                        .createQueryBuilder(
                          WhatsappNumberHost,
                          'whatsapp_number_host',
                        )
                        .andWhere('id != :id', {
                          id: botAvailable[countingSend]['id'],
                        })
                        .orderBy('RANDOM()')
                        .getOne();

                      whatsappBotSendMessage(
                        `${botAvailable[countingSend]['number']}@c.us`,
                        {
                          message: faker.lorem.text(),
                          typeActivity:
                            WhatsappNumberHostActivityType.SEND_MESSAGE_TO_ANOTHER_BOT,
                        },
                        randomHost,
                      );
                    } catch (error) {}

                    countingSend++;
                  }
                }
              }
            } catch (error) {}
          }
        }
      }
    }
  }

  // Scheduler untuk cek status
  @Cron('0 0 * * * *', {
    timeZone: 'Asia/Jakarta',
  })
  async cekStatus() {
    if (parseInt(process.env.NODE_APP_INSTANCE) === 0) {
      const hosts = await WhatsappNumberHost.find();

      let text = 'Status whatsapp bot';

      for await (const host of hosts) {
        let type = '';

        switch (host.type) {
          case WhatsappNumberHostType.GROUP:
            type = 'Membuat Grup';
            break;

          case WhatsappNumberHostType.BROADCAST_PROMO:
            type = 'Blaster';
            break;

          case WhatsappNumberHostType.ACTIVITY_GROUP:
            type = 'Pengumpulan Data';
            break;

          case WhatsappNumberHostType.BROADCAST:
            type = 'Broadcast Pesan';
            break;

          default:
            break;
        }

        text += `
          
Nama : ${host.name}
Nomer : ${host.number}
Tipe : ${type}
Aktif : ${host.isActive ? 'Iya' : 'Tidak'}
Status: ${host.status}`;
      }

      if (text) {
        const host = await getConnection()
          .createQueryBuilder(WhatsappNumberHost, 'whatsapp_number_host')
          .andWhere('whatsapp_number_host.status = :connected', {
            connected: WhatsappNumberHostStatus.CONNECTED,
          })
          .orderBy('RANDOM()')
          .getOne();

        const notificationGroupId = (
          await Setting.findOne({ where: { key: 'NOTIFICATION_GROUP_ID' } })
        ).value;

        whatsappBotSendMessage(notificationGroupId, { message: text }, host);
      }
    }
  }
}
