import {
  BadRequestException,
  Body,
  Controller,
  Headers,
  Post,
  UnauthorizedException,
  UsePipes,
  Query,
  Get,
  UseGuards
} from '@nestjs/common';
import { I18nValidationPipe } from 'src/libs/pipes/validation.pipe';
import { whatsappBotSendMessage } from 'src/libs/utils/bot-whatsapp.utils';
import { FetchWhatsappSendMessageDTO, WhatsappSendMessageDTO } from '../dto/whatsapp.dto';
import {
  WhatsappNumberHostStatus,
  WhatsappNumberHostType,
} from '../entities/whatsapp-number-host.entity';
import { WhatsappNumberHostService } from '../services/whatsapp-number-host.service';
import { convertToPhoneNumber } from 'src/libs/utils/string-transform';
import { WhatsappSendMessage } from '../entities/whatsapp-send-message.entity';
import { formatResponse } from 'src/libs/utils/response-format.utils';
import { Filter } from 'src/libs/utils/filter.utils';
import { getConnection } from 'typeorm';
import { Roles } from 'src/libs/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/libs/guards/roles.guard';

@Controller('whatsapps')
export class WhatsappController {
  constructor(
    private readonly whatsappNumberHostService: WhatsappNumberHostService,
  ) {}

  @Get()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async fetch(@Query() params: FetchWhatsappSendMessageDTO) {
    const { fields } = params;

    const query = await getConnection().createQueryBuilder(WhatsappSendMessage,
      'whatsapp_send_message',
    ).leftJoinAndSelect('whatsapp_send_message.host', 'host');

    const filterQuery = new Filter(
      query,
      params,
      ['whatsapp_send_message.phoneNumber'],
      'whatsapp_send_message',
    );

    const total = await filterQuery.count();
    const queryResult = await filterQuery.generate(true);
    let data = await queryResult.getMany();

    if (fields) data = formatResponse(data, fields, true);

    return {
      messgae: 'Success get list data whatsapp messages',
      data: { total, data }
    }
  }

  @Post('send-message')
  @UsePipes(I18nValidationPipe)
  async sendMessage(
    @Body() payload: WhatsappSendMessageDTO,
    @Headers() headers,
  ) {
    const token =
      '$2a$12$i9NWnu6iNoyPsCz5goZOPuAC9BHb3nnCICpI1NH//59jUzvON7Jyu';
    const { authorization } = headers;

    if (!authorization)
      throw new UnauthorizedException(
        'The token on authorization cannot be empty',
      );

    if (authorization.split(' ')[1] !== token)
      throw new UnauthorizedException("Tokens don't match");

    let host = await this.whatsappNumberHostService.whatsappNumberHostRepository.findOne(
      {
        where: {
          isActive: true,
          status: WhatsappNumberHostStatus.CONNECTED,
          type: WhatsappNumberHostType.BROADCAST,
        },
      },
    );

    if (!host) {
      host = await this.whatsappNumberHostService.whatsappNumberHostRepository.findOne(
        {
          where: {
            isActive: true,
            status: WhatsappNumberHostStatus.CONNECTED,
            type: WhatsappNumberHostType.BROADCAST_PROMO,
          },
        },
      );
    }

    if (!host) throw new BadRequestException('No active host number found');

    const { phoneNumber, text, image } = payload;

    try {
      const createSendMessage = new WhatsappSendMessage();
      createSendMessage.host = host;
      createSendMessage.phoneNumber = convertToPhoneNumber(phoneNumber);
      createSendMessage.text = text;
      createSendMessage.image = image;
      await createSendMessage.save();
    } catch (error) {}

    await whatsappBotSendMessage(
      `${convertToPhoneNumber(phoneNumber)}@c.us`,
      { message: text, image },
      host,
    );

    return {
      message: 'Message sent successfully, currently status in delivery queue',
    };
  }
}
