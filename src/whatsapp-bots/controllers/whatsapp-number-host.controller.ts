import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/libs/decorators/roles.decorator';
import { RolesGuard } from 'src/libs/guards/roles.guard';
import { SentryInterceptor } from 'src/libs/interceptors/sentry.interceptors';
import { I18nValidationPipe } from 'src/libs/pipes/validation.pipe';
import {
  CreateWhatsappNumberHostDTO,
  FetchWhatsappNumberHostActivityFilterDTO,
  FetchWhatsappNumberHostFilterDTO,
  SetStatusWhatsappNumberHostDTO,
} from '../dto/whatsapp-number-host.dto';
import { WhatsappNumberHostActivityStatus } from '../entities/whatsapp-number-host-activity.entity';
import { WhatsappNumberHostService } from '../services/whatsapp-number-host.service';

@Controller('whatsapp-number-hosts')
export class WhatsappNumberHostController {
  constructor(
    private readonly whatsappNumberHostService: WhatsappNumberHostService,
  ) {}

  @Get()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async fetch(@Query() params: FetchWhatsappNumberHostFilterDTO) {
    return {
      message: 'Berhasil mendapatkan data group',
      result: await this.whatsappNumberHostService.fetch(params),
    };
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async findById(@Param('id') id: string) {
    return {
      message: 'Berhasil mendapatkan data group',
      result: await this.whatsappNumberHostService.findById(id),
    };
  }

  @Get('/:id/activities')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async findActivityById(
    @Param('id') id: string,
    @Query() filter: FetchWhatsappNumberHostActivityFilterDTO,
  ) {
    return {
      message: 'Berhasil mendapatkan data aktivitas nomer master',
      result: await this.whatsappNumberHostService.findActivityById(id, filter),
    };
  }

  @Post()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async store(@Body() payload: CreateWhatsappNumberHostDTO) {
    return {
      message: 'Berhasil membuat data group',
      result: await this.whatsappNumberHostService.create(payload),
    };
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async update(
    @Body() payload: CreateWhatsappNumberHostDTO,
    @Param('id') id: string,
  ) {
    return {
      message: 'Berhasil merubah data group',
      result: await this.whatsappNumberHostService.update(payload, id),
    };
  }

  @Patch('/:id/set-status')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async setStatus(
    @Body() payload: SetStatusWhatsappNumberHostDTO,
    @Param('id') id: string,
  ) {
    return {
      message: 'Berhasil merubah data group',
      result: await this.whatsappNumberHostService.setStatus(payload, id),
    };
  }

  @Patch('/:token/set-status-by-token')
  @UsePipes(I18nValidationPipe)
  async setStatusByToken(
    @Body() payload: SetStatusWhatsappNumberHostDTO,
    @Param('token') token: string,
  ) {
    return {
      message: 'Berhasil merubah data group',
      result: await this.whatsappNumberHostService.setStatusByToken(
        payload,
        token,
      ),
    };
  }

  @Patch('/:id/update-activity')
  @UsePipes(I18nValidationPipe)
  async updateActivity(
    @Body()
    payload: { status: WhatsappNumberHostActivityStatus; notes: string },
    @Param('id') id: string,
  ) {
    return {
      message: 'Berhasil merubah data group',
      result: await this.whatsappNumberHostService.updateActivity(id, payload),
    };
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async destroy(@Param('id') id: string) {
    return {
      message: 'Berhasil menghapus data group',
      result: await this.whatsappNumberHostService.destroy(id),
    };
  }
}
