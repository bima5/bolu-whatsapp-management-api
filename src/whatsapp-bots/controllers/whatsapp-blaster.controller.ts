import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/libs/decorators/roles.decorator';
import { RolesGuard } from 'src/libs/guards/roles.guard';
import { I18nValidationPipe } from 'src/libs/pipes/validation.pipe';
import {
  CreateWhatsappBlasterDTO,
  FetchFilterWhatsappBlasterDTO,
  FetchFilterWhatsappBlasterQueueDTO,
  UpdatePuaseWHatsappBlasterDTO,
} from '../dto/whatsapp-blaster.dto';
import { WhatsappBlasterService } from '../services/whatsapp-blaster.service';

@Controller('whatsapp-blasters')
export class WhatsappBlasterController {
  constructor(
    private readonly whatsappBlasterService: WhatsappBlasterService,
  ) {}

  @Get('/get-audiences')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async getAudiences(
    @Query() query: { templateIds: string; countOnly: boolean },
  ) {
    const { templateIds, countOnly } = query;

    return {
      message: 'Berhasil mendapatkan data audience',
      result: await this.whatsappBlasterService.filterAudience(
        templateIds.split(','),
        countOnly,
      ),
    };
  }

  @Get('/get-audiences-by-query')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async getAudiencesByQuery(@Query() query: any) {
    return {
      message: 'Berhasil mendapatkan data audience',
      result: await this.whatsappBlasterService.filterAudienceByQueryy(query),
    };
  }

  @Patch('/update-queue')
  async updateQueue(@Body() payload: any) {
    return {
      message: 'Berhasil update queue blaster',
      result: await this.whatsappBlasterService.updateQueue(payload),
    };
  }

  @Patch('/update-seen-at-blaster')
  async updateSeenAtBlaster(@Body() payload: any) {
    return {
      message: 'Berhasil update queue blaster',
      result: await this.whatsappBlasterService.updateSeenAtBlaster(payload),
    };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async fetch(@Param() params: FetchFilterWhatsappBlasterDTO) {
    return {
      message: 'Berhasil mendapatkan data blaster',
      result: await this.whatsappBlasterService.fetch(params),
    };
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async findById(@Param('id') id: string) {
    return {
      message: 'Berhasil mendapatkan data blaster',
      result: await this.whatsappBlasterService.findById(id),
    };
  }

  @Get('/:id/download-report')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async downloadReport(@Param('id') id: string) {
    return {
      message: 'Success download report blaster',
      result: await this.whatsappBlasterService.downloadReport(id),
    };
  }

  @Get('/:id/queue')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async findQueueByBlasterId(
    @Param('id') id: string,
    @Query() params: FetchFilterWhatsappBlasterQueueDTO,
  ) {
    return {
      message: 'Berhasil mendapatkan data blaster',
      result: await this.whatsappBlasterService.findQueueByBlasterId(
        id,
        params,
      ),
    };
  }

  @Post()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async store(@Body() payload: CreateWhatsappBlasterDTO) {
    return {
      message: 'Berhasil membuat data blaster',
      result: await this.whatsappBlasterService.create(payload),
    };
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async update(
    @Body() payload: CreateWhatsappBlasterDTO,
    @Param('id') id: string,
  ) {
    return {
      message: 'Berhasil merubah data blaster',
      result: await this.whatsappBlasterService.update(payload, id),
    };
  }

  @Patch('/:id/update-pause')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UsePipes(I18nValidationPipe)
  @Roles(['ADMIN'])
  async updatePause(
    @Body() payload: UpdatePuaseWHatsappBlasterDTO,
    @Param('id') id: string,
  ) {
    return {
      message: 'Berhasil merubah data blaster',
      result: await this.whatsappBlasterService.updatePause(payload, id),
    };
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(['ADMIN'])
  async destroy(@Param('id') id: string) {
    return {
      message: 'Berhasil menghapus data group',
      result: await this.whatsappBlasterService.destroy(id),
    };
  }
}
