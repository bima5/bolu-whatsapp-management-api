import { CacheModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsModule } from 'src/settings/settings.module';
import { UsersModule } from 'src/users/users.module';
import { RunGetSendMessageCommand } from './commands/run-get-sent message.command';
import { WhatsappBlasterController } from './controllers/whatsapp-blaster.controller';
import { WhatsappNumberHostController } from './controllers/whatsapp-number-host.controller';
import { WhatsappController } from './controllers/whatsapp.controller';
import { WhatsappBlasterQueueRepository } from './repositories/whatsapp-blaster-queue.repository';
import { WhatsappBlasterRepository } from './repositories/whatsapp-blaster.repository';
import { WhatsappNumberHostActivityRepository } from './repositories/whatsapp-number-host-activity.repository';
import { WhatsappNumberHostHistoryRepository } from './repositories/whatsapp-number-host-status.repository';
import { WhatsappNumberHostRepository } from './repositories/whatsapp-number-host.repository';
import { WhatsappBlasterService } from './services/whatsapp-blaster.service';
import { WhatsappNumberHostService } from './services/whatsapp-number-host.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      WhatsappNumberHostRepository,
      WhatsappNumberHostHistoryRepository,
      WhatsappBlasterRepository,
      WhatsappNumberHostActivityRepository,
      WhatsappBlasterQueueRepository,
    ]),

    // Chache Module
    CacheModule.register(),

    UsersModule,
    SettingsModule,
  ],
  controllers: [
    WhatsappNumberHostController,
    WhatsappBlasterController,
    WhatsappController,
  ],
  providers: [
    WhatsappNumberHostService,
    WhatsappBlasterService,
    RunGetSendMessageCommand,
  ],
  exports: [WhatsappNumberHostService],
})
export class WhatsappBotsModule {}
