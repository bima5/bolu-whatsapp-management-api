import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappBlasterFilterTemplate } from './whatsapp-blaster-filter-template.entity';

export enum WhatsappBlasterFilterTemplateOptionKey {
  UTM = 'UTM',
  COURSE = 'COURSE',
  AVERAGE_PROFIT_PER_MONTH = 'AVERAGE_PROFIT_PER_MONTH',
  CATEGORY = 'CATEGORY',
  TYPE_SELLING = 'TYPE_SELLING',
  PRODUCT_CATEGORY = 'PRODUCT_CATEGORY',
  PROVINCE = 'PROVINCE',
  CITY = 'CITY',
  SUBDISTRICT = 'SUBDISTRICT',
  GENDER = 'GENDER',
  AGE = 'AGE',
  LTV = 'LTV',
  ALREADY_JOIN_COMMUNITY_GROUP = 'ALREADY_JOIN_COMMUNITY_GROUP',
  IS_PAID_MEMBER = 'IS_PAID_MEMBER',
}

@Entity()
export class WhatsappBlasterFilterTemplateOption extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  key: WhatsappBlasterFilterTemplateOptionKey;

  @Column()
  value: string;

  @ManyToOne(
    () => WhatsappBlasterFilterTemplate,
    (whatsappBlasterFilterTemplate: WhatsappBlasterFilterTemplate) =>
      whatsappBlasterFilterTemplate.options,
  )
  template: WhatsappBlasterFilterTemplate;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
