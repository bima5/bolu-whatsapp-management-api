import { User } from 'src/users/users.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { WhatsappBlasterQueueHistoryStatus } from './whatsapp-blaster-queue-history-status.entity';
import { WhatsappBlaster } from './whatsapp-blaster.entity';
import { WhatsappNumberHost } from './whatsapp-number-host.entity';

export enum WhatsappBlasterQueueStatus {
  PENDING = 'PENDING',
  PROGRESS = 'PROGRESS',
  DONE = 'DONE',
  FAILED = 'FAILED',
}

@Entity()
export class WhatsappBlasterQueue extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column()
  phoneNumber: string;

  @Column()
  text: string;

  @Column({ nullable: true })
  image?: string;

  @Column({ nullable: true })
  note?: string;

  @Column({ default: WhatsappBlasterQueueStatus.PENDING })
  status: WhatsappBlasterQueueStatus;

  @Column({ nullable: true })
  messageWhatsappId: string;

  @Column({ nullable: true, type: 'timestamp' })
  seenAt: Date;

  @ManyToOne(() => User, (user: User) => user.blasterQueues, {
    onDelete: 'SET NULL',
    nullable: true,
  })
  user: User;

  @ManyToOne(
    () => WhatsappBlaster,
    (whatsappBlaster: WhatsappBlaster) => whatsappBlaster.blasterQueues,
    {
      onDelete: 'CASCADE',
    },
  )
  blaster: WhatsappBlaster;

  @ManyToOne(
    () => WhatsappNumberHost,
    (whatsappNumberHost: WhatsappNumberHost) =>
      whatsappNumberHost.blasterQueues,
    {
      onDelete: 'SET NULL',
      nullable: true,
    },
  )
  host: WhatsappNumberHost;

  @OneToMany(
    () => WhatsappBlasterQueueHistoryStatus,
    (whatsappBlasterQueueHistoryStatus: WhatsappBlasterQueueHistoryStatus) =>
      whatsappBlasterQueueHistoryStatus.blasterQueue,
  )
  historyStatuses: WhatsappBlasterQueueHistoryStatus[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
