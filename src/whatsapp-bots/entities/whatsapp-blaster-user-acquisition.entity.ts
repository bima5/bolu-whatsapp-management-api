import { User } from 'src/users/users.entity';
import {
  BaseEntity,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappBlaster } from './whatsapp-blaster.entity';

@Entity()
export class WhatsappBlasterUserAcquisition extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => User, (user: User) => user.blasterAcquisitions)
  user: User;

  @ManyToOne(
    () => WhatsappBlaster,
    (user: WhatsappBlaster) => user.userAcquisitions,
  )
  blaster: WhatsappBlaster;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
