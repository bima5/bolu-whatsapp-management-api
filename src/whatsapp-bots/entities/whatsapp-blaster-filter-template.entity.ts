import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappBlasterFilterTemplateOption } from './whatsapp-blaster-filter-template-option.entity';
import { WhatsappBlaster } from './whatsapp-blaster.entity';

@Entity()
export class WhatsappBlasterFilterTemplate extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @ManyToMany(
    () => WhatsappBlaster,
    (whatsappBlaster: WhatsappBlaster) => whatsappBlaster.templates,
  )
  blasters: WhatsappBlaster[];

  @OneToMany(
    () => WhatsappBlasterFilterTemplateOption,
    (
      whatsappBlasterFilterTemplateOption: WhatsappBlasterFilterTemplateOption,
    ) => whatsappBlasterFilterTemplateOption.template,
  )
  options: WhatsappBlasterFilterTemplateOption[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
