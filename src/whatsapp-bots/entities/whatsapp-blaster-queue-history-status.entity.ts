import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import {
  WhatsappBlasterQueue,
  WhatsappBlasterQueueStatus,
} from './whatsapp-blaster-queue.entity';

@Entity()
export class WhatsappBlasterQueueHistoryStatus extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: WhatsappBlasterQueueStatus;

  @ManyToOne(
    () => WhatsappBlasterQueue,
    (whatsappBlasterQueue: WhatsappBlasterQueue) =>
      whatsappBlasterQueue.historyStatuses,
    {
      onDelete: 'CASCADE',
    },
  )
  blasterQueue: WhatsappBlasterQueue;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
