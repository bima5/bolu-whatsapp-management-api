import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappBlasterFilterTemplate } from './whatsapp-blaster-filter-template.entity';
import { WhatsappBlasterQueue } from './whatsapp-blaster-queue.entity';
import { WhatsappBlasterUserAcquisition } from './whatsapp-blaster-user-acquisition.entity';

export enum WhatsappBlasterStatus {
  DRAFT = 'DRAFT',
  PUBLISHED = 'PUBLISHED',
}

@Entity()
export class WhatsappBlaster extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  code: string;

  @Column()
  text: string;

  @Column({ nullable: true })
  image?: string;

  @Column({ default: WhatsappBlasterStatus.DRAFT })
  status: WhatsappBlasterStatus;

  @Column({ nullable: true })
  csv?: string;

  @Column({ default: 0 })
  totalQueue: number;

  @Column({ default: 0 })
  totalQueueDone: number;

  @Column({ nullable: true, type: 'timestamptz' })
  startAt: Date;

  @Column({ nullable: true, type: 'timestamptz' })
  endAt: Date;

  @Column({ default: false })
  isFinished: boolean;

  @Column({ type: 'timestamptz', nullable: true })
  finishedAt: Date;

  @Column({ default: false })
  isPaused: boolean;

  @Column({ nullable: true })
  note?: string;

  @OneToMany(
    () => WhatsappBlasterQueue,
    (whatsappBlasterQueue: WhatsappBlasterQueue) =>
      whatsappBlasterQueue.blaster,
  )
  blasterQueues: WhatsappBlasterQueue[];

  @OneToMany(
    () => WhatsappBlasterUserAcquisition,
    (whatsappBlasterUserAcquisition: WhatsappBlasterUserAcquisition) =>
      whatsappBlasterUserAcquisition.blaster,
  )
  userAcquisitions: WhatsappBlasterUserAcquisition;

  @ManyToMany(
    () => WhatsappBlasterFilterTemplate,
    (whatsappBlasterFilterTemplate: WhatsappBlasterFilterTemplate) =>
      whatsappBlasterFilterTemplate.blasters,
  )
  @JoinTable()
  templates: WhatsappBlasterFilterTemplate[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
