import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappNumberHost } from './whatsapp-number-host.entity';

export enum WhatsappNumberHostActivityType {
  SEND_MESSAGE = 'SEND_MESSAGE',
  SEND_MESSAGE_BLASTER = 'SEND_MESSAGE_BLASTER',
  SEND_MESSAGE_TO_ANOTHER_BOT = 'SEND_MESSAGE_TO_ANOTHER_BOT',
  SYNC_MESSAGE = 'SYNC_MESSAGE',
  SYNC_MEMBER = 'SYNC_MEMBER',
  CREATE_GROUP = 'CREATE_GROUP',
  INVITE_MEMBER = 'INVITE_MEMBER',
  REMOVE_MEMBER = 'REMOVE_MEMBER',
  GET_MESSAGE_STATUS = 'GET_MESSAGE_STATUS',
  SET_GROUP_DESCRIPTION = 'SET_GROUP_DESCRIPTION',
  SEND_MESSAGE_GREETING = 'SEND_MESSAGE_GREETING',
  SET_ALLOW_SEND_MESSAGE_GROUP = 'SET_ALLOW_SEND_MESSAGE_GROUP',
  SET_ALLOW_CHANGE_INFO_GROUP = 'SET_ALLOW_CHANGE_INFO_GROUP',
  SET_MEMBER_GROUP_TO_ADMIN = 'SET_MEMBER_GROUP_TO_ADMIN',
}

export enum WhatsappNumberHostActivityStatus {
  PROGRESS = 'PROGRESS',
  DONE = 'DONE',
  FAILED = 'FAILED',
}

@Entity()
export class WhatsappNumberHostActivity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => WhatsappNumberHost,
    (whatsappNumberHost: WhatsappNumberHost) => whatsappNumberHost.activities,
    {
      onDelete: 'CASCADE',
    },
  )
  host: WhatsappNumberHost;

  @Column({ nullable: true })
  type: WhatsappNumberHostActivityType;

  @Column({ default: WhatsappNumberHostActivityStatus.PROGRESS })
  status: WhatsappNumberHostActivityStatus;

  @Column({ nullable: true })
  notes: string;

  @Column({ nullable: true })
  url: string;

  @Column({ nullable: true })
  payload: string;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
