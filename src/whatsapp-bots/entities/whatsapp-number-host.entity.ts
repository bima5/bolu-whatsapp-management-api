import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WhatsappBlasterQueue } from './whatsapp-blaster-queue.entity';
import { WhatsappNumberHostActivity } from './whatsapp-number-host-activity.entity';
import { WhatsappSendMessage } from './whatsapp-send-message.entity';

export enum WhatsappNumberHostStatus {
  CONFLICT = 'CONFLICT',
  CONNECTED = 'CONNECTED',
  DEPRECATED_VERSION = 'DEPRECATED_VERSION',
  OPENING = 'OPENING',
  PAIRING = 'PAIRING',
  PROXYBLOCK = 'PROXYBLOCK',
  SMB_TOS_BLOCK = 'SMB_TOS_BLOCK',
  TIMEOUT = 'TIMEOUT',
  TOS_BLOCK = 'TOS_BLOCK',
  UNLAUNCHED = 'UNLAUNCHED',
  UNPAIRED = 'UNPAIRED',
  DISCONNECTED = 'DISCONNECTED',
  NOT_CONNECTED = 'NOT_CONNECT',
}

export enum WhatsappNumberHostType {
  BROADCAST = 'BROADCAST',
  BROADCAST_PROMO = 'BROADCAST_PROMO',
  GROUP = 'GROUP',
  ACTIVITY_GROUP = 'ACTIVITY_GROUP',
}

@Entity()
export class WhatsappNumberHost extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  name?: string;

  @Column()
  number: string;

  @Column()
  token: string;

  @Column({ default: true })
  isActive: boolean;

  @Column({ default: WhatsappNumberHostType.GROUP })
  type: WhatsappNumberHostType;

  @Column({ nullable: true })
  host?: string;

  @Column({ nullable: true })
  ipHost: string;

  @Column({ nullable: true })
  ipPortHost: string;

  @Column({ default: WhatsappNumberHostStatus.NOT_CONNECTED })
  status: WhatsappNumberHostStatus;

  @Column({ default: 0 })
  totalGroup: number;

  // Optional Information
  @Column({ nullable: true })
  device?: string;

  @Column({ nullable: true })
  location?: string;

  @Column({ nullable: true, type: 'timestamptz' })
  expiredAt?: Date;

  @OneToMany(
    () => WhatsappNumberHostActivity,
    (whatsappNumberHostActivity: WhatsappNumberHostActivity) =>
      whatsappNumberHostActivity.host,
  )
  activities: WhatsappNumberHostActivity[];

  @OneToMany(
    () => WhatsappBlasterQueue,
    (whatsappBlasterQueue: WhatsappBlasterQueue) => whatsappBlasterQueue.host,
  )
  blasterQueues: WhatsappBlasterQueue[];

  @OneToMany(
    () => WhatsappSendMessage,
    (whatsappSendMessage: WhatsappSendMessage) => whatsappSendMessage.host,
  )
  messages: WhatsappSendMessage[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
    name: 'updated_at',
  })
  updatedAt: Date;
}
