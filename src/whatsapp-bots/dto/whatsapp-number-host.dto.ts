import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import {
  WhatsappNumberHostActivityStatus,
  WhatsappNumberHostActivityType,
} from '../entities/whatsapp-number-host-activity.entity';
import {
  WhatsappNumberHostStatus,
  WhatsappNumberHostType,
} from '../entities/whatsapp-number-host.entity';

export class FetchWhatsappNumberHostFilterDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;

  @IsOptional()
  type?: WhatsappNumberHostType;

  @IsOptional()
  status?: WhatsappNumberHostStatus;
}

export class CreateWhatsappNumberHostDTO {
  @IsOptional()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  number: string;

  @IsNotEmpty()
  @IsString()
  token: string;

  @IsNotEmpty()
  @IsString()
  host: string;

  @IsNotEmpty()
  @IsString()
  type: WhatsappNumberHostType;

  @IsOptional()
  isActive?: boolean;

  @IsOptional()
  device?: string;

  @IsOptional()
  location?: string;

  @IsOptional()
  expiredAt?: Date;

  @IsOptional()
  ipHost?: string;

  @IsOptional()
  ipPortHost?: string;
}

export class SetStatusWhatsappNumberHostDTO {
  @IsNotEmpty()
  status: WhatsappNumberHostStatus;
}

export class FetchWhatsappNumberHostActivityFilterDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;

  @IsOptional()
  type?: WhatsappNumberHostActivityStatus;

  @IsOptional()
  status?: WhatsappNumberHostActivityType;
}
