import { IsNotEmpty, IsOptional } from 'class-validator';
import { WhatsappBlasterStatus } from '../entities/whatsapp-blaster.entity';

export class FetchFilterWhatsappBlasterDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;
}

export class CreateWhatsappBlasterDTO {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  status: WhatsappBlasterStatus;

  @IsNotEmpty()
  text: string;

  @IsOptional()
  image?: string;

  @IsNotEmpty()
  templateIds: string[];

  @IsOptional()
  csv?: string;

  @IsOptional()
  startAt?: Date;

  @IsOptional()
  endAt?: Date;
}

export class UpdatePuaseWHatsappBlasterDTO {
  @IsNotEmpty()
  isPaused: boolean;
}

export class FetchFilterWhatsappBlasterQueueDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;

  @IsOptional()
  alreadyRead?: boolean;

  @IsOptional()
  status?: WhatsappBlasterStatus;
}
