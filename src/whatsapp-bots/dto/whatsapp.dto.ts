import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class FetchWhatsappSendMessageDTO {
  @IsOptional()
  ordering?: string;

  @IsOptional()
  search?: string;

  @IsOptional()
  page?: number;

  @IsOptional()
  limit?: number;

  @IsOptional()
  fields?: string;
}

export class WhatsappSendMessageDTO {
  @IsNotEmpty()
  text: string;

  @IsNotEmpty()
  phoneNumber: string;

  @IsOptional()
  image?: string;
}
