import { Injectable } from '@nestjs/common';
import * as moment from 'moment-timezone';
import { Command } from 'nestjs-command';
import { whatsappBotGetMessageStatus } from 'src/libs/utils/bot-whatsapp.utils';
import { IsNull, MoreThanOrEqual, Not } from 'typeorm';
import {
  WhatsappBlasterQueue,
  WhatsappBlasterQueueStatus,
} from '../entities/whatsapp-blaster-queue.entity';

@Injectable()
export class RunGetSendMessageCommand {
  @Command({ command: 'seeder:run-get-send-message' })
  async store() {
    const queues = await WhatsappBlasterQueue.find({
      where: {
        status: WhatsappBlasterQueueStatus.DONE,
        updatedAt: MoreThanOrEqual(
          new Date(moment().subtract(7, 'd').format('YYYY-MM-DD')),
        ),
        messageWhatsappId: Not(IsNull()),
        seenAt: IsNull(),
      },
      relations: ['host'],
    });

    if (queues.length > 0) {
      for await (const queue of queues) {
        await whatsappBotGetMessageStatus(
          `${queue.phoneNumber}@c.us`,
          queue.messageWhatsappId,
          queue.host,
          queue.id,
        );
      }
    }
  }
}
