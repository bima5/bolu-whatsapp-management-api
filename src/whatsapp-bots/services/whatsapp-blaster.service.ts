import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { WhatsappBlasterRepository } from 'src/whatsapp-bots/repositories/whatsapp-blaster.repository';

import { User } from 'src/users/users.entity';
import { getConnection, getManager, In, IsNull, Not } from 'typeorm';
import { WhatsappBlasterFilterTemplate } from '../entities/whatsapp-blaster-filter-template.entity';
import {
  WhatsappBlasterFilterTemplateOption,
  WhatsappBlasterFilterTemplateOptionKey,
} from '../entities/whatsapp-blaster-filter-template-option.entity';
import { UsersService } from 'src/users/services/users.service';
import {
  WhatsappBlaster,
  WhatsappBlasterStatus,
} from '../entities/whatsapp-blaster.entity';
import {
  CreateWhatsappBlasterDTO,
  FetchFilterWhatsappBlasterDTO,
  FetchFilterWhatsappBlasterQueueDTO,
  UpdatePuaseWHatsappBlasterDTO,
} from '../dto/whatsapp-blaster.dto';
import { formatResponse } from 'src/libs/utils/response-format.utils';
import { Filter } from 'src/libs/utils/filter.utils';
import {
  WhatsappBlasterQueue,
  WhatsappBlasterQueueStatus,
} from '../entities/whatsapp-blaster-queue.entity';
import { WhatsappNumberHost } from '../entities/whatsapp-number-host.entity';
import { WhatsappBlasterQueueHistoryStatus } from '../entities/whatsapp-blaster-queue-history-status.entity';
import { convertCsvToArray } from 'src/libs/utils/csv.utils';
import { convertToPhoneNumber } from 'src/libs/utils/string-transform';
import { upload } from 'src/libs/utils/upload.utils';
import slugify from 'slugify';
import { v4 as uuidv4 } from 'uuid';
import { WhatsappBlasterQueueRepository } from '../repositories/whatsapp-blaster-queue.repository';
import { Excel } from 'src/libs/utils/excel.utils';
import { DateHelper } from 'src/libs/utils/date.utils';

@Injectable()
export class WhatsappBlasterService {
  constructor(
    @InjectRepository(WhatsappBlasterRepository)
    private readonly whatsappBlasterRepository: WhatsappBlasterRepository,

    @InjectRepository(WhatsappBlasterQueueRepository)
    private readonly whatsappBlasterQueueRepository: WhatsappBlasterQueueRepository,

    private readonly userService: UsersService,
  ) {}

  async filterAudienceByQueryy(query) {
    const filters = [];

    const keys = Object.keys(query);

    for await (const key of keys) {
      filters.push({ key, value: query[key].split(',') });
    }

    const total = await this.filterUsers(filters, true);

    return {
      total,
    };
  }

  async filterAudience(
    templateIds: string[],
    countOnly?: boolean,
  ): Promise<{ data?: User[]; total: number }> {
    const filters = [];
    const relations = [];
    const where: any = {};

    for await (const templateId of templateIds) {
      const templateOptions = await WhatsappBlasterFilterTemplateOption.find({
        where: {
          template: await WhatsappBlasterFilterTemplate.findOne(templateId),
        },
      });

      for await (const option of templateOptions) {
        const { key, value } = option;

        const index = filters.findIndex((filter) => filter.key === key);

        if (index > -1) {
          const arrSplit = filters[index].value.split(',');

          for await (const val of value.split(',')) {
            const indexSplit = arrSplit.indexOf(val);

            if (indexSplit === -1) arrSplit.push(val);
          }

          filters[index].value = arrSplit;
        } else {
          filters.push({ key, value: value.split(',') });
        }
      }
    }

    const data = await this.filterUsers(filters, countOnly);

    if (countOnly) {
      console.log(data);
      return {
        data: [],
        total: data,
      };
    } else {
      return {
        data,
        total: data.length,
      };
    }
  }

  async filterUsers(filters, countOnly?: boolean) {
    const wheres = [];
    const joins = [];

    const joinField = [];

    for await (const filter of filters) {
      const { key, value } = filter;

      switch (key) {
        case WhatsappBlasterFilterTemplateOptionKey.UTM:
          if (joinField.indexOf('utms') === -1) {
            joins.push(
              `LEFT OUTER JOIN utm_users_user ON "user".id = utm_users_user.user_id`,
            );
            joinField.push('utms');
          }
          wheres.push(
            `utm_users_user.utm_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.COURSE:
          if (joinField.indexOf('courses') === -1) {
            joins.push(
              'inner join user_course on "user".id = user_course.user_id',
            );
            joinField.push('courses');
          }
          wheres.push(
            `user_course.course_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.PROVINCE:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.province_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.CITY:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.city_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.SUBDISTRICT:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.subdistrict_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.ALREADY_JOIN_COMMUNITY_GROUP:
          if (value.length === 1) {
            if (value[0] === 'true') {
              wheres.push(`"user".whatsapp_group_community_id is not null`);
            } else {
              wheres.push(`"user".whatsapp_group_community_id is null`);
            }
          }
          break;

        case WhatsappBlasterFilterTemplateOptionKey.AVERAGE_PROFIT_PER_MONTH:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.average_profit_per_month IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.TYPE_SELLING:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.type_selling IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.PRODUCT_CATEGORY:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }
          wheres.push(
            `user_profile.category_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.CATEGORY:
          if (joinField.indexOf('categories') === -1) {
            joins.push(
              `LEFT OUTER JOIN user_categories_category ON "user".id = user_categories_category.user_id`,
            );
            joinField.push('categories');
          }
          wheres.push(
            `user_categories_category.category_id IN (${value
              .map((value) => `'${value}'`)
              .join(',')})`,
          );
          break;

        case WhatsappBlasterFilterTemplateOptionKey.IS_PAID_MEMBER:
          if (joinField.indexOf('profile') === -1) {
            joins.push(
              'inner join user_profile on "user".profile_id = user_profile.id',
            );
            joinField.push('profile');
          }

          if (value.length === 1) {
            wheres.push(`user_profile.is_paid_member = '${value[0]}'`);
          }
          break;

        default:
          break;
      }
    }

    if (countOnly) {
      const sql = `
      SELECT count("user".id) FROM "user" ${joins.join(
        ' ',
      )} WHERE ${wheres.join(' AND ')}
    `;

      return (await getManager().query(sql))[0].count;
    } else {
      const sql = `
      SELECT * FROM "user" ${joins.join(' ')} WHERE ${wheres.join(' AND ')}
    `;

      return await getManager().query(sql);
    }
  }

  async fetch(
    params: FetchFilterWhatsappBlasterDTO,
  ): Promise<{ total: number; data: WhatsappBlaster[] }> {
    const { fields } = params;

    const query = this.whatsappBlasterRepository.createQueryBuilder(
      'whatsapp_blaster',
    );
    const filterQuery = new Filter(
      query,
      params,
      ['whatsapp_blaster.name'],
      'whatsapp_blaster',
      [],
    );

    const total = await filterQuery.count();
    const queryResult = await filterQuery.generate(true);
    let data = await queryResult.getMany();

    if (fields) data = formatResponse(data, fields, true);

    for await (const d of data) {
      d.totalRead = await WhatsappBlasterQueue.count({
        where: { blaster: d, seenAt: Not(IsNull()) },
      });
    }

    return { total, data };
  }

  async createAudienceBlaster(
    blaster: WhatsappBlaster,
    csv?: string,
  ): Promise<WhatsappBlaster> {
    if (csv) {
      const { results, path }: any = await convertCsvToArray(
        csv,
        'blaster',
        true,
      );

      const uploadFile = new upload();
      const csvFile = await uploadFile.uploadImage(
        path,
        'csv',
        slugify(`${blaster.name} ${uuidv4()}`),
        'csv',
        'files',
        true,
      );
      await uploadFile.removeFile(path);

      blaster.csv = csvFile;

      for await (const result of results) {
        const { name, whatsapp, note } = result;

        const user = await User.findOne({
          where: { phoneNumber: convertToPhoneNumber(whatsapp) },
        });

        let firstName = '';
        let lastName = '';

        if (!user) {
          firstName = name.split(' ')[0];
          lastName = name.split(' ').slice(1, name.split(' ').length).join(' ');
        } else {
          firstName = user.firstName || name.split(' ')[0];
          lastName =
            user.lastName ||
            name.split(' ').slice(1, name.split(' ').length).join(' ');
        }

        const queue = new WhatsappBlasterQueue();
        queue.blaster = blaster;
        queue.firstName = firstName;
        queue.lastName = lastName;
        queue.phoneNumber = convertToPhoneNumber(whatsapp);
        queue.user = user;
        queue.image = blaster.image;
        queue.note = note;
        queue.text = blaster.text
          .replace(/{firstName}/g, firstName)
          .replace(/{last_name}/g, lastName)
          .replace(/{note}/g, note)
          .replace(/{name}/g, `${firstName} ${lastName}`)
          .replace(/{code}/g, blaster.code);
        await queue.save();
      }

      blaster.totalQueue = results.length;
      await blaster.save();
    } else {
      const { data, total }: any = await this.filterAudience(
        blaster.templates.map((template) => template.id),
      );

      for await (const user of data) {
        const queue = new WhatsappBlasterQueue();
        queue.blaster = blaster;
        queue.firstName = user.first_name;
        queue.lastName = user.last_name;
        queue.phoneNumber = user.phone_number;
        queue.user = await User.findOne({ id: user.id });
        queue.image = blaster.image;
        queue.text = blaster.text
          .replace(/{firstName}/g, user.first_name)
          .replace(/{last_name}/g, user.last_name)
          .replace(/{name}/g, `${user.first_name} ${user.last_name}`)
          .replace(/{code}/g, blaster.code)
          .replace(/{note}/g, blaster.note);
        await queue.save();
      }

      blaster.totalQueue = total;
      await blaster.save();
    }

    return blaster;
  }

  async create(payload: CreateWhatsappBlasterDTO): Promise<WhatsappBlaster> {
    const { status, csv } = payload;

    let blaster = await this.whatsappBlasterRepository.createOrUpdate(payload);

    if (status === WhatsappBlasterStatus.PUBLISHED) {
      const exists = await WhatsappBlasterQueue.findOne({ blaster });

      if (!exists) {
        blaster = await this.createAudienceBlaster(blaster, csv);
      }
    }

    return blaster;
  }

  async update(
    payload: CreateWhatsappBlasterDTO,
    id: string,
  ): Promise<WhatsappBlaster> {
    const { status, csv } = payload;

    let blaster = await this.findById(id);

    blaster = await this.whatsappBlasterRepository.createOrUpdate(payload, id);

    if (status === WhatsappBlasterStatus.PUBLISHED) {
      const exists = await WhatsappBlasterQueue.findOne({ blaster });

      if (!exists) {
        blaster = await this.createAudienceBlaster(blaster, csv);
      } else {
        const queues = await WhatsappBlasterQueue.find({
          where: { blaster, status: WhatsappBlasterQueueStatus.PENDING },
        });

        for await (const queue of queues) {
          const { firstName, lastName } = queue;

          queue.text = blaster.text
            .replace(/{firstName}/g, firstName)
            .replace(/{last_name}/g, lastName)
            .replace(/{name}/g, `${firstName} ${lastName}`)
            .replace(/{code}/g, blaster.code);
          queue.image = blaster.image;
          await queue.save();
        }
      }
    }

    return blaster;
  }

  async destroy(id: string): Promise<void> {
    const blaster = await this.findById(id);

    await this.whatsappBlasterRepository.destroy(id);
  }

  async findById(id: string): Promise<WhatsappBlaster> {
    const blaster: any = await this.whatsappBlasterRepository.findById(id);

    return blaster;
  }

  // Update queue
  async updateQueue(payload) {
    const { whatsappMessageId, token, id } = payload;

    const whatsappNumberHost = await WhatsappNumberHost.findOne({ token });

    if (!whatsappNumberHost) throw new ForbiddenException('Token not valid');

    const queue = await WhatsappBlasterQueue.findOne({
      where: { id },
      relations: ['blaster'],
    });

    if (!queue) throw new NotFoundException('Queue not found');

    queue.messageWhatsappId = whatsappMessageId;
    queue.status = WhatsappBlasterQueueStatus.DONE;
    await queue.save();

    const queueHistory = await WhatsappBlasterQueueHistoryStatus.findOne({
      where: { blasterQueue: queue },
    });
    queueHistory.name = queue.status;
    await queueHistory.save();

    // queue.blaster.totalQueueDone = await WhatsappBlasterQueue.count({
    //   blaster: queue.blaster,
    //   status: WhatsappBlasterQueueStatus.DONE,
    // });
    if (queue.blaster.totalQueueDone >= queue.blaster.totalQueue) {
      queue.blaster.isFinished = true;
      queue.blaster.finishedAt = new Date();
    }
    // queue.blaster.save();

    return queue;
  }

  async updateSeenAtBlaster(payload: any): Promise<void> {
    const { whatsappMessageId, token, blastetQueueId, reads } = payload;

    if (reads.read.length > 0) {
      const whatsappNumberHost = await WhatsappNumberHost.findOne({ token });

      if (!whatsappNumberHost) throw new ForbiddenException('Token not valid');

      const queue = await WhatsappBlasterQueue.findOne({
        where: { id: blastetQueueId, messageWhatsappId: whatsappMessageId },
      });

      if (!queue) throw new ForbiddenException('Queue not found');

      queue.seenAt = reads.read[0].t;
      await queue.save();
    }
  }

  /**
   * @param  {string} id
   * @param  {FetchFilterWhatsappBlasterQueueDTO} params
   * @returns WhatsappBlasterQueue
   */
  async findQueueByBlasterId(
    id: string,
    params: FetchFilterWhatsappBlasterQueueDTO,
  ): Promise<{ total: number; data: WhatsappBlasterQueue[] }> {
    const { fields } = params;

    const query = this.whatsappBlasterQueueRepository
      .createQueryBuilder('whatsapp_blaster_queue')
      .andWhere('whatsapp_blaster_queue.blaster_id = :id', { id })
      .leftJoinAndSelect('whatsapp_blaster_queue.host', 'host')
      .leftJoinAndSelect(
        'whatsapp_blaster_queue.historyStatuses',
        'historyStatuses',
      );

    const filterQuery = new Filter(
      query,
      params,
      [
        'whatsapp_blaster_queue.firstName',
        'whatsapp_blaster_queue.lastName',
        'whatsapp_blaster_queue.phoneNumber',
      ],
      'whatsapp_blaster_queue',
      [],
    );

    const total = await filterQuery.count();
    const queryResult = await filterQuery.generate(true);
    let data = await queryResult.getMany();

    if (fields) data = formatResponse(data, fields, true);

    return { total, data };
  }

  /**
   * @param  {UpdatePuaseWHatsappBlasterDTO} payload
   * @param  {string} id
   * @returns Promise
   */
  async updatePause(
    payload: UpdatePuaseWHatsappBlasterDTO,
    id: string,
  ): Promise<WhatsappBlaster> {
    const blaster = await this.findById(id);

    blaster.isPaused = payload.isPaused;
    await blaster.save();

    return blaster;
  }

  async downloadReport(id: string): Promise<string> {
    const excel = new Excel();
    const sheets = [
      {
        name: 'Sheet 1',
        headers: [
          {
            label: 'Nama depan',
            key: 'firstName',
          },
          {
            label: 'Nama belakang',
            key: 'lastName',
          },
          {
            label: 'Whatsapp',
            key: 'phoneNumber',
          },
          {
            label: 'Text',
            key: 'text',
          },
          {
            label: 'Dilihat pada',
            key: 'seenAt',
          },
          {
            label: 'Status',
            key: 'status',
          },
          {
            label: 'Catatan',
            key: 'note',
          },
        ],
        rows: [],
      },
    ];

    const blaster = await WhatsappBlaster.findOne(id);

    const data = await WhatsappBlasterQueue.find({
      where: { blaster },
    });

    sheets[0].rows = data;

    return excel.exportExcel(
      sheets,
      slugify(`blaster queue export ${blaster.name}`),
    );
  }
}
