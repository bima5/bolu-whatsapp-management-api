import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { whatsappBotSendMessage } from 'src/libs/utils/bot-whatsapp.utils';
import { Filter } from 'src/libs/utils/filter.utils';
import { Pusher } from 'src/libs/utils/notification.utils';
import { formatResponse } from 'src/libs/utils/response-format.utils';
import { SettingsService } from 'src/settings/settings.service';
import { User } from 'src/users/users.entity';
import {
  CreateWhatsappNumberHostDTO,
  FetchWhatsappNumberHostActivityFilterDTO,
  FetchWhatsappNumberHostFilterDTO,
  SetStatusWhatsappNumberHostDTO,
} from '../dto/whatsapp-number-host.dto';
import {
  WhatsappNumberHostActivity,
  WhatsappNumberHostActivityStatus,
} from '../entities/whatsapp-number-host-activity.entity';
import { WhatsappNumberHostHistory } from '../entities/whatsapp-number-host-history.entity';
import {
  WhatsappNumberHost,
  WhatsappNumberHostStatus,
  WhatsappNumberHostType,
} from '../entities/whatsapp-number-host.entity';
import { WhatsappNumberHostActivityRepository } from '../repositories/whatsapp-number-host-activity.repository';
import { WhatsappNumberHostRepository } from '../repositories/whatsapp-number-host.repository';

@Injectable()
export class WhatsappNumberHostService {
  constructor(
    @InjectRepository(WhatsappNumberHostRepository)
    public readonly whatsappNumberHostRepository: WhatsappNumberHostRepository,

    @InjectRepository(WhatsappNumberHostActivityRepository)
    public readonly whatsappNumberHostActivityRepository: WhatsappNumberHostActivityRepository,

    private readonly settingServie: SettingsService,
  ) {}

  async fetch(
    params: FetchWhatsappNumberHostFilterDTO,
  ): Promise<{ total: number; data: WhatsappNumberHost[] }> {
    const { fields, type, status } = params;

    const query = this.whatsappNumberHostRepository.createQueryBuilder(
      'whatsapp_number_host',
    );

    if (type) query.andWhere(`whatsapp_number_host.type = :type`, { type });
    if (status)
      query.andWhere(`whatsapp_number_host.status = :status`, { status });

    const filterQuery = new Filter(
      query,
      params,
      ['whatsapp_number_host.number'],
      'whatsapp_number_host',
    );

    const total = await filterQuery.count();
    const queryResult = await filterQuery.generate(true);
    let data = await queryResult.getMany();

    if (fields) data = formatResponse(data, fields, true);

    return { total, data };
  }

  async create(
    payload: CreateWhatsappNumberHostDTO,
  ): Promise<WhatsappNumberHost> {
    const group = await this.whatsappNumberHostRepository.createOrUpdate(
      payload,
    );

    return group;
  }

  async update(
    payload: CreateWhatsappNumberHostDTO,
    id: string,
  ): Promise<WhatsappNumberHost> {
    let group = await this.findById(id);

    group = await this.whatsappNumberHostRepository.createOrUpdate(payload, id);

    return group;
  }

  async destroy(id: string): Promise<void> {
    const group = await this.findById(id);

    await this.whatsappNumberHostRepository.destroy(id);
  }

  async findById(id: string): Promise<WhatsappNumberHost> {
    const group: any = await this.whatsappNumberHostRepository.findById(id);

    return group;
  }

  async setStatus(
    payload: SetStatusWhatsappNumberHostDTO,
    id: string,
  ): Promise<WhatsappNumberHost> {
    const { status } = payload;

    const number = await this.findById(id);

    number.status = status;
    await number.save();

    const history = new WhatsappNumberHostHistory();
    history.status = status;
    await history.save();

    return number;
  }

  async setStatusByToken(
    payload: SetStatusWhatsappNumberHostDTO,
    token: string,
  ): Promise<WhatsappNumberHost> {
    const { status } = payload;

    const number = await this.whatsappNumberHostRepository.findOne({
      where: { token },
    });

    number.status = status;
    await number.save();

    const pusher = new Pusher();
    pusher.send('host-number', `change-status`, {
      id: number.id,
      status: number.status,
    });

    const history = new WhatsappNumberHostHistory();
    history.status = status;
    await history.save();

    return number;
  }

  async getNumberHostActive(
    type: WhatsappNumberHostType,
  ): Promise<WhatsappNumberHost> {
    return await WhatsappNumberHost.findOne({
      where: {
        type,
        status: WhatsappNumberHostStatus.CONNECTED,
        isActive: true,
      },
      order: { totalGroup: 'ASC' },
    });
  }

  async updateActivity(
    id,
    payload: { status: WhatsappNumberHostActivityStatus; notes: string },
  ): Promise<WhatsappNumberHostActivity> {
    const activity = await WhatsappNumberHostActivity.findOne(id);
    const { status, notes } = payload;

    if (!activity) throw new NotFoundException('Aktivitas tidak ditemukan');

    activity.status = status;
    activity.notes = notes;
    await activity.save();

    return activity;
  }

  // API untuk mendapatkan list aktivitas nomer master
  // Require param ID Nomer Master
  // Return Array Aktivitas
  async findActivityById(
    numberHostId: string,
    filter: FetchWhatsappNumberHostActivityFilterDTO,
  ): Promise<{ total: number; data: WhatsappNumberHostActivity[] }> {
    const { fields, type, status } = filter;

    const query = this.whatsappNumberHostActivityRepository
      .createQueryBuilder('whatsapp_number_host_activity')
      .leftJoin('whatsapp_number_host_activity.host', 'host')
      .andWhere('host.id = :numberHostId', { numberHostId });

    if (type)
      query.andWhere(`whatsapp_number_host_activity.type = :type`, { type });

    if (status)
      query.andWhere(`whatsapp_number_host_activity.status = :status`, {
        status,
      });

    const filterQuery = new Filter(
      query,
      filter,
      ['host.name'],
      'whatsapp_number_host_activity',
    );

    const total = await filterQuery.count();
    const queryResult = await filterQuery.generate(true);
    let data = await queryResult.getMany();

    if (fields) data = formatResponse(data, fields, true);

    return { total, data };
  }
}
