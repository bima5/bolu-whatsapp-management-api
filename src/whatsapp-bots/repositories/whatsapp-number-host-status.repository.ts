import { EntityRepository, Repository } from 'typeorm';
import { WhatsappNumberHostHistory } from '../entities/whatsapp-number-host-history.entity';

@EntityRepository(WhatsappNumberHostHistory)
export class WhatsappNumberHostHistoryRepository extends Repository<WhatsappNumberHostHistory> {}
