import { NotFoundException } from '@nestjs/common';
import { generateRandomString } from 'src/libs/utils/string-transform';
import { upload } from 'src/libs/utils/upload.utils';
import { EntityRepository, Repository } from 'typeorm';
import { CreateWhatsappBlasterDTO } from '../dto/whatsapp-blaster.dto';
import { WhatsappBlasterFilterTemplate } from '../entities/whatsapp-blaster-filter-template.entity';
import { WhatsappBlaster } from '../entities/whatsapp-blaster.entity';

@EntityRepository(WhatsappBlaster)
export class WhatsappBlasterRepository extends Repository<WhatsappBlaster> {
  async findById(id: string): Promise<WhatsappBlaster> {
    const blaster: any = await this.findOne({
      where: { id },
      relations: ['templates'],
    });

    if (!blaster)
      throw new NotFoundException('Blaster whatsapp tidak ditemukan');

    return blaster;
  }

  async createOrUpdate(
    payload: CreateWhatsappBlasterDTO,
    id?: string,
  ): Promise<WhatsappBlaster> {
    const { name, text, image, status, templateIds, startAt, endAt } = payload;

    let blaster: any = new WhatsappBlaster();

    if (id) blaster = await this.findById(id);

    blaster.name = name;
    blaster.text = text;
    blaster.status = status;
    if (startAt) blaster.startAt = startAt;
    if (endAt) blaster.endAt = endAt;
    blaster.code = generateRandomString(5);

    if (templateIds.length > 0) {
      blaster.templates = [];

      for await (const templateId of templateIds) {
        const template = await WhatsappBlasterFilterTemplate.findOne(
          templateId,
        );

        if (template) blaster.templates.push(template);
      }
    }

    if (image) {
      if (image.split('base64').length > 1) {
        const imagePath = await new upload().uploadImage(
          image,
          'blaster',
          blaster.slug,
        );
        blaster.image = imagePath;
      }
    }

    await blaster.save();

    return blaster;
  }

  async destroy(id: string): Promise<void> {
    const data = await this.findById(id);

    await this.delete(id);
  }
}
