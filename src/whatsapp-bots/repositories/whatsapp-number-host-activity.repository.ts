import { EntityRepository, Repository } from 'typeorm';
import { WhatsappNumberHostActivity } from '../entities/whatsapp-number-host-activity.entity';

@EntityRepository(WhatsappNumberHostActivity)
export class WhatsappNumberHostActivityRepository extends Repository<WhatsappNumberHostActivity> {}
