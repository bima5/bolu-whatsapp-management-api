import { NotFoundException } from '@nestjs/common';
import { convertToPhoneNumber } from 'src/libs/utils/string-transform';
import { EntityRepository, Repository } from 'typeorm';
import { CreateWhatsappNumberHostDTO } from '../dto/whatsapp-number-host.dto';
import { WhatsappNumberHost } from '../entities/whatsapp-number-host.entity';

@EntityRepository(WhatsappNumberHost)
export class WhatsappNumberHostRepository extends Repository<WhatsappNumberHost> {
  async findById(id: string): Promise<WhatsappNumberHost> {
    const partner = await this.findOne({
      where: { id },
    });

    if (!partner)
      throw new NotFoundException('Nomer host whatsapp tidak ditemukan');

    return partner;
  }

  async createOrUpdate(
    payload: CreateWhatsappNumberHostDTO,
    id?: string,
  ): Promise<WhatsappNumberHost> {
    const {
      name,
      number,
      token,
      host,
      type,
      isActive,
      device,
      location,
      expiredAt,
      ipHost,
      ipPortHost,
    } = payload;

    let whatsappNumberHost = new WhatsappNumberHost();

    if (id) whatsappNumberHost = await this.findById(id);

    whatsappNumberHost.name = name;
    whatsappNumberHost.number = convertToPhoneNumber(number);
    whatsappNumberHost.token = token;
    whatsappNumberHost.host = host;
    whatsappNumberHost.type = type;
    whatsappNumberHost.isActive = isActive;
    whatsappNumberHost.device = device;
    whatsappNumberHost.location = location;
    if (expiredAt) whatsappNumberHost.expiredAt = expiredAt;
    whatsappNumberHost.ipHost = ipHost;
    whatsappNumberHost.ipPortHost = ipPortHost;

    await whatsappNumberHost.save();

    return whatsappNumberHost;
  }

  async destroy(id: string): Promise<void> {
    const data = await this.findById(id);

    await this.delete(id);
  }
}
