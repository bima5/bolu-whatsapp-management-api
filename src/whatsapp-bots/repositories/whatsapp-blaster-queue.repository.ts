import { EntityRepository, Repository } from 'typeorm';
import { WhatsappBlasterQueue } from '../entities/whatsapp-blaster-queue.entity';

@EntityRepository(WhatsappBlasterQueue)
export class WhatsappBlasterQueueRepository extends Repository<WhatsappBlasterQueue> {}
