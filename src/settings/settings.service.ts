import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Setting } from './settings.entity';
import { SettingRepository } from './settings.repository';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(SettingRepository)
    private readonly settingRepository: SettingRepository,
  ) {}

  async fetch(): Promise<Setting[]> {
    return await this.settingRepository.find();
  }

  async patch(payloads): Promise<Setting[]> {
    const settings = [];

    for await (const payload of payloads) {
      const setting = await Setting.findOne(payload['id']);

      if (setting) {
        setting.value = payload['value'];
        await setting.save();

        settings.push(setting);
      }
    }

    return settings;
  }

  async getSettingByKey(key: string): Promise<Setting> {
    return await Setting.findOne({ key });
  }
}
