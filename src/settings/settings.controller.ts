import { Body, Controller, Get, Patch, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/libs/decorators/roles.decorator';
import { RolesGuard } from 'src/libs/guards/roles.guard';
import { SettingsService } from './settings.service';

@Controller('settings')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class SettingsController {
  constructor(private readonly settingService: SettingsService) {}

  @Get('/')
  @Roles(['ADMIN'])
  async fetch() {
    const result = await this.settingService.fetch();

    return { message: 'Berhasil mendapatkan pengaturan', result };
  }

  @Patch()
  @Roles(['ADMIN'])
  async patch(@Body() payload) {
    const result = await this.settingService.patch(payload);

    return { message: 'Berhasil mengubah pengaturan', result };
  }
}
