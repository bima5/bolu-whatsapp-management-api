import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';
import { Setting } from '../settings.entity';

@Injectable()
export class SettingCommand {
  @Command({ command: 'seeder:setting', describe: 'Seeder untuk setting' })
  async store() {
    const data = [
      {
        key: 'NOTIFICATION_GROUP_ID',
        description: 'Grup ID untuk proses notifikasi status bot',
        group: 'WHATSAPP_BOT',
        value: '120363039655686002@g.us',
      },
      {
        key: 'NOTIFICATION_JOIN_LEAVE_GROUP_ID',
        description: 'Grup ID untuk proses notifikasi status bot',
        group: 'WHATSAPP_BOT',
        value: '120363039655686002@g.us',
      },
    ];

    for await (const d of data) {
      const exists = await Setting.findOne({ key: d['key'] });

      if (!exists) {
        const setting = new Setting();
        setting.key = d['key'];
        setting.description = d['description'];
        setting.group = d['group'];
        setting.value = d['value'];

        await setting.save();
      }
    }
  }
}
