import { UserRole } from 'src/users/users.entity';

export interface JwtPayload {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  roles: UserRole[];
  alreadyUpdatePersonalIdentity: boolean;
}
