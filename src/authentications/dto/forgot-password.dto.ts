import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { UserExistsByEmail } from 'src/libs/validator/user-exists-by-email.validator';

export class ForgotPasswordDTO {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @UserExistsByEmail()
  email: string;
}
