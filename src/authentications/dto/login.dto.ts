import { IsNotEmpty, IsString } from 'class-validator';
import { UserExistsByPhoneNumberOrEmail } from 'src/libs/validator/user-exists-by-phone-number-or-email.validator';

export class LoginDTO {
  @IsNotEmpty()
  @IsString()
  @UserExistsByPhoneNumberOrEmail()
  username: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
