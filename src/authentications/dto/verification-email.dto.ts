import { IsNotEmpty } from 'class-validator';

export class VerificationEmailDTO {
  @IsNotEmpty()
  secret: string;
}
