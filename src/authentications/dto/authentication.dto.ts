import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class LoginGoogleDTO {
  @IsNotEmpty()
  idToken: string;

  @IsOptional()
  utm: string;

  @IsOptional()
  utms: string;
}

export class RefreshTokenDTO {
  @IsNotEmpty()
  userId: string;

  @IsNotEmpty()
  refreshToken: string;
}
