import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { NewPasswordNotSameWithOldPassword } from 'src/libs/validator/new-password-not-same-with-old-password.validator';

export class UpdatePasswordDTO {
  @IsNotEmpty()
  @IsString()
  secret: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message:
      'Password terlalu pendek, panjang minimal adalah $constraint1 karakter',
  })
  @MaxLength(16, {
    message:
      'Password terlalu panjang, panjang maksimal adalah $constraint1 karakter',
  })
  password: string;
}
