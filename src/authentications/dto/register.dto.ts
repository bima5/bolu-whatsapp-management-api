import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { UniqueEmail } from 'src/libs/validator/unique-email.validator';
import { UniquePhoneNumber } from 'src/libs/validator/unique-phone-number.validator';
import { UniqueUsername } from 'src/libs/validator/unique-username.validator';

export class RegisterDTO {
  // @IsNotEmpty()
  // @IsString()
  // @UniqueUsername()
  // username: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6, {
    message:
      'Password terlalu pendek, panjang minimal adalah $constraint1 karakter',
  })
  @MaxLength(16, {
    message:
      'Password terlalu panjang, panjang maksimal adalah $constraint1 karakter',
  })
  password: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  // @IsEmail()
  @UniqueEmail()
  email?: string;

  @IsNotEmpty()
  @IsString()
  @IsMobilePhone('id-ID')
  @UniquePhoneNumber()
  phoneNumber?: string;

  @IsOptional()
  utm: string;

  @IsOptional()
  utms: string;

  profilePicture?: string;
}
