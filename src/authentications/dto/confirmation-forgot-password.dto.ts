import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ConfirmationForgotPasswordDTO {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  pin: string;
}
