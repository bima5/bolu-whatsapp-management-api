require('dotenv').config();

import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/users/repositories/users.repository';
import { LoginDTO } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt.interface';
import { User } from 'src/users/users.entity';
import { UpdatePasswordDTO } from './dto/update-password.dto';
import { UsersService } from 'src/users/services/users.service';
import { RefreshTokenDTO } from './dto/authentication.dto';
import * as bcrypt from 'bcrypt';
import jwt_decode from 'jwt-decode';
import { isJwtExpired } from 'jwt-check-expiration';

@Injectable()
export class AuthenticationsService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
    private userService: UsersService,
  ) {}

  async login(
    loginDto: LoginDTO,
    type?: string,
  ): Promise<{ accessToken: string }> {
    const user = await this.userRepository.validateUserPassword(loginDto);

    if (!user) {
      throw new UnauthorizedException(
        'Nomor hp atau email tidak cocok dengan kata sandi',
      );
    }

    if (
      !user.profile &&
      user.roles.findIndex((role) => role.name === 'ADMIN') === -1
    )
      throw new BadRequestException({
        message: 'user has not completed identity',
        data: {
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          phoneNumber: user.phoneNumber,
        },
      });
    await this.userRepository.updateFirstLoginTime(user.id);
    await this.userRepository.updateLastLoginTime(user.id);

    return await this.getAccessTokenAndRefreshToken(user);
  }

  async getAccessTokenAndRefreshToken(
    user: User,
    dontRefreshCurrentHashedRefreshToken?: boolean,
    defaultHashedRefreshToken?: string,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    try {
      const { firstName, lastName, email, username, id, roles } = user;

      const alreadyUpdatePersonalIdentity = !user.profile === undefined;

      const payloadJwt: JwtPayload = {
        firstName,
        lastName,
        email,
        username,
        roles,
        id,
        alreadyUpdatePersonalIdentity,
      };
      let currentHashedRefreshToken = await bcrypt.hash(
        `${id}${new Date()}`,
        10,
      );

      if (dontRefreshCurrentHashedRefreshToken) {
        currentHashedRefreshToken = defaultHashedRefreshToken;
      }

      const accessToken = this.jwtService.sign(payloadJwt, {
        secret: process.env.JWT_ACCESS_TOKEN_SECRET,
        expiresIn: parseInt(process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME) * 2,
      });
      const refreshToken = this.jwtService.sign(
        { ...payloadJwt, currentHashedRefreshToken },
        {
          secret: process.env.JWT_REFRESH_TOKEN_SECRET,
          expiresIn:
            parseInt(process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME) * 4,
        },
      );

      await this.userService.userRepository.update(user.id, {
        currentHashedRefreshToken,
      });

      return { accessToken, refreshToken };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async refreshToken(
    payload: RefreshTokenDTO,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    const { refreshToken, userId } = payload;
    const user = await this.userService.getUserById(userId);

    if (isJwtExpired(refreshToken)) {
      throw new UnauthorizedException('Token has been expired');
    }

    const { currentHashedRefreshToken }: any = jwt_decode(refreshToken);

    if (!user) {
      throw new UnauthorizedException('User not found');
    }

    if (currentHashedRefreshToken === user.currentHashedRefreshToken) {
      return await this.getAccessTokenAndRefreshToken(
        user,
        true,
        currentHashedRefreshToken,
      );
    } else {
      throw new UnauthorizedException('Invalid token');
    }
  }
}
