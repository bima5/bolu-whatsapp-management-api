import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { I18nRequestScopeService } from 'nestjs-i18n';
import { I18nValidationPipe } from 'src/libs/pipes/validation.pipe';
import { UsersService } from 'src/users/services/users.service';
import { User } from 'src/users/users.entity';
import { AuthenticationsService } from './authentications.service';
import { LoginDTO } from './dto/login.dto';
import { GetUser } from './get-user.decorator';

@Controller('authentications')
export class AuthenticationsController {
  constructor(
    private authenticationService: AuthenticationsService,
    private userService: UsersService,
    private readonly i18n: I18nRequestScopeService,
  ) {}

  @Post('/login')
  @UsePipes(I18nValidationPipe)
  async login(@Body() loginDto: LoginDTO, @Query() query: any) {
    const { type } = query;

    const result = await this.authenticationService.login(loginDto, type);

    return {
      message: await this.i18n.translate('response.authentication.login'),
      result,
    };
  }

  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  async me(@GetUser() user: User) {
    return {
      message: await this.i18n.translate('response.authentication.me'),
      result: await this.userService.getUserById(user.id),
    };
  }
}
