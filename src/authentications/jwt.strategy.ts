require('dotenv').config();

import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/users/repositories/users.repository';
import { JwtPayload } from './jwt.interface';
import { decrypted } from 'src/libs/utils/hashing.utils';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_ACCESS_TOKEN_SECRET,
    });
  }

  async validate(payload: JwtPayload) {
    const { id } = payload;

    const user = await this.userRepository
      .createQueryBuilder('user')
      .andWhere('user.id = :id', { id })
      .leftJoinAndSelect('user.roles', 'roles')
      .getOne();

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
