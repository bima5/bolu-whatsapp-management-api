import { createParamDecorator } from '@nestjs/common';
import { User } from 'src/users/users.entity';

export const GetUser = createParamDecorator(
  (data, req): User => {
    return req.switchToHttp().getRequest().user;
  },
);
