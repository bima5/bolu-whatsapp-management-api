require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from './libs/interceptors/transform.interceptors';
import * as bodyParser from 'body-parser';
import { useContainer } from 'class-validator';
import * as Sentry from '@sentry/node';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  Sentry.init({
    dsn: process.env.DSN_SENTRY,
  });

  app.enableCors();
  app.setGlobalPrefix('v1');
  app.useGlobalInterceptors(new TransformInterceptor());
  app.use(bodyParser.json({ limit: '30mb' }));
  app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  await app.listen(process.env.PORT, '0.0.0.0');
}
bootstrap();
