import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import { CommandModule } from 'nestjs-command';

// Module aplikasi
import { AuthenticationsModule } from './authentications/authentications.module';
import { ConfigurationsModule } from './configurations/configurations.module';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { TasksModule } from './tasks/tasks.module';
import { SettingsModule } from './settings/settings.module';
import { SettingCommand } from './settings/commands/seed-setting.command';
import { WhatsappBotsModule } from './whatsapp-bots/whatsapp-bots.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      serveRoot: '/media',
    }),
    I18nModule.forRoot({
      fallbackLanguage: 'id',
      parser: I18nJsonParser,
      parserOptions: {
        path: join(__dirname, '/i18n/'),
        // add this to enable live translations
        watch: true,
      },
    }),

    // Untuk custom command
    CommandModule,

    // Scheduler
    ScheduleModule.forRoot(),

    TasksModule,
    UsersModule,
    AuthenticationsModule,
    ConfigurationsModule,
    SettingsModule,
    WhatsappBotsModule,
  ],

  providers: [SettingCommand],
})
export class AppModule {}
