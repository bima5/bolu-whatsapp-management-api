module.exports = {
  apps: [
    {
      name: 'LMS API - Staging',
      script: './dist/main.js',
      instances: 4,
    },
  ],
};
